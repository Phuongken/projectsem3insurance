﻿namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HistoryContract")]
    public partial class HistoryContract
    {
        public int ID { get; set; }

        public int ContractID { get; set; }
        public int UserID { get; set; }

        public string ContractName { get; set; }
        public decimal? TotalPrice { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [StringLength(250)]
        public string PaymentAddress { get; set; }

        [StringLength(250)]
        public string DeliveryAddress { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int CreateBy { get; set; }

        public int UpdateBy { get; set; }

        public int? Status { get; set; }
    }
}
