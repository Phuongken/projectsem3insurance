﻿namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("New")]
    public partial class New
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Tên không được bỏ trống")]
        [DisplayName("Tên")]
        [StringLength(150)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Tiêu đề không được bỏ trống")]
        [DisplayName("Tiêu đề")]
        [StringLength(50)]
        public string Title { get; set; }

        [Column(TypeName = "text")]
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        [DisplayName("Nội dung")]
        public string Content { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public int CreateBy { get; set; }

        public int UpdateBy { get; set; }

        [Required(ErrorMessage = "Ảnh không được bỏ trống")]
        [DisplayName("Ảnh đại diện")]
        [StringLength(250)]
        public string Image { get; set; }
        public int CategoryID { get; set; }
    }
}
