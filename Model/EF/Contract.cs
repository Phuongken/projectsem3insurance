﻿namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Contract")]
    public partial class Contract
    {
        public int ID { get; set; }
        [DisplayName("Mã người dùng")]
        public int UserID { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DisplayName("Tên hợp đồng")]
        public string ContractName { get; set; }
        public decimal? TotalPrice { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DisplayName("Ngày bắt đầu")]
        public DateTime? StartDate { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DisplayName("Ngày kết thúc")]
        public DateTime? EndDate { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DisplayName("Địa chỉ thanh toán")]
        [StringLength(250)]
        public string PaymentAddress { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DisplayName("Địa chỉ giao hàng")]
        [StringLength(250)]
        public string DeliveryAddress { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int CreateBy { get; set; }

        public int UpdateBy { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DisplayName("Trạng thái")]
        public int? Status { get; set; }
    }
}
