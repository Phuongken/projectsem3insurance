﻿namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InsuranceCategory")]
    public partial class InsuranceCategory
    {
        public int ID { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        [DisplayName("Tên thể loại")]
        public string Name { get; set; }

        public int Status { get; set; }
    }
}
