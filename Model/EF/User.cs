namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("User")]
    public partial class User
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Username { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }

        
        public string Salt { get; set; }

        
        public string Name { get; set; }

        [Required]
        [StringLength(250)]
        public string Email { get; set; }

        
        public string Phone { get; set; }

       
        public string Avatar { get; set; }

        
        public string Address { get; set; }

        public DateTime Dob { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public int CreateBy { get; set; }

        public int UpdateBy { get; set; }

        public int Status { get; set; }

        public int Gender { get; set; }
    }
}
