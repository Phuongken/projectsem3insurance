﻿namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Insurance")]
    public partial class Insurance
    {
        public int ID { get; set; }
        [Required(ErrorMessage ="Tên không được bỏ trống")]
        [DisplayName("Tên bảo hiểm")]
        [StringLength(150)]
        public string Name { get; set; }
        [Required(ErrorMessage = "Avatar không được bỏ trống")]
        [DisplayName("Ảnh đại diện")]
        [StringLength(250)]
        public string Avatar { get; set; }
        [Required(ErrorMessage = "Giá tiền không được bỏ trống")]
        [DisplayName("Số tiền bảo hiểm")]
        public decimal? Price { get; set; }
        [Required(ErrorMessage = "không được bỏ trống")]
        [DisplayName("Số tiền giới hạn của bảo hiểm")]
        public decimal? LimitPrice { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DisplayName("Thể loại bảo hiểm")]
        public int InsuranceCategoryID { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DisplayName("Kỳ hạn nhỏ nhất.")]
        public int? MinTerm { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [DisplayName("Kỳ hạn lớn nhất")]
        public int? MaxTerm { get; set; }
        
        public double FeeInsurance { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public int CreateBy { get; set; }

        public int UpdateBy { get; set; }

        public int Status { get; set; }
    }
}
