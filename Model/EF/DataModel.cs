namespace Model.EF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DataModel : DbContext
    {
        public DataModel()
            : base("name=DataModel")
        {
        }

        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<CategoryNew> CategoryNews { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<HistoryContract> HistoryContracts { get; set; }
        public virtual DbSet<HistoryOrderDetail> HistoryOrderDetails { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<InfoOnline> InfoOnlines { get; set; }
        public virtual DbSet<Insurance> Insurances { get; set; }
        public virtual DbSet<InsuranceCategory> InsuranceCategories { get; set; }
        public virtual DbSet<New> News { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<PayMent> PayMents { get; set; }
        public virtual DbSet<PolicyInsurance> PolicyInsurances { get; set; }
        public virtual DbSet<PolicyInsuranceCategory> PolicyInsuranceCategories { get; set; }
        public virtual DbSet<Provind> Provinds { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Ward> Wards { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Insurance>()
                .Property(e => e.Price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Insurance>()
                .Property(e => e.LimitPrice)
                .HasPrecision(18, 0);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.Price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.AddressContract)
                .IsFixedLength();
        }
    }
}
