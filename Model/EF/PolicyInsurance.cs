﻿namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PolicyInsurance")]
    public partial class PolicyInsurance
    {
        public int ID { get; set; }
        [DisplayName("Mã bảo hiểm")]
        public int InsuranceID { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        [DisplayName("Nội dung")]
        public string Content { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public int CreateBy { get; set; }

        public int UpdateBy { get; set; }
        [DisplayName("Loại chính sách")]
        public int PolicyInsuranceCategoryID { get; set; }
    }
}
