namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Address")]
    public partial class Address
    {
        public int ID { get; set; }

        public int ProvindID { get; set; }

        public int DistrictID { get; set; }

        public int WardID { get; set; }
    }
}
