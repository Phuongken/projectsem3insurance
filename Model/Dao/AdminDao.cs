﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Model.Dao
{
    public class AdminDao
    {
        DataModel db = null;
        public AdminDao()
        {
            db = new DataModel();
        }
        public bool Register(Admin admin)
        {
            try
            {
                if (db.Admins.Any(x => x.Username == admin.Username))
                {
                    return false;
                }
                string salt = Guid.NewGuid().ToString().Substring(0, 7);
                string pass = admin.Password;

                string MD5Hash = Encryptor.MD5Hash(pass + salt);

                admin.Salt = salt;
                admin.Password = MD5Hash;
                admin.ConfirmPassword = MD5Hash;
                admin.CreateDate = DateTime.Now;
                admin.UpdateDate = DateTime.Now;
                admin.Status = 1;

                db.Admins.Add(admin);

                db.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Login(string userName, string passWord)
        {
            var result = db.Admins.SingleOrDefault(x => x.Username == userName);
            if (result == null)
            {
                return false;
            }
            else
            {
                if (result.Status == 0)
                {
                    return false;
                }
                else
                {
                    string salt = result.Salt.ToString();

                    var MD5Hash = Encryptor.MD5Hash(passWord + salt);

                    if (result.Password == MD5Hash)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        public Admin GetById(string userName)
        {
            return db.Admins.SingleOrDefault(x => x.Username == userName);
        }

        public Employee find(int? id)
        {
            return db.Employees.Find(id);
        }
        public bool Edit(Employee emp)
        {
            try
            {
                var editAdmin = db.Employees.Find(emp.ID);
                editAdmin.ID = emp.ID;
                editAdmin.Name = emp.Name;
                editAdmin.Address = emp.Address;
                editAdmin.Phone = emp.Phone;
                editAdmin.Avatar = emp.Avatar;
                editAdmin.Role = emp.Role;
                editAdmin.Email = emp.Email;
                editAdmin.UpdateDate = DateTime.Now;
            
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(int? id)
        {
            try
            {
                var delete = db.Employees.Find(id);
                delete.Status = 0;
                db.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<Employee> listEmployee()
        {
            List<Employee> result = db.Employees.Where(x => x.Status == 1).ToList();
            return result;
        }
    }
}
