﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class ClientDao
    {

        DataModel db = null;

        public ClientDao()
        {
            db = new DataModel();
        }

        public InsuranceCategory NameCategory(int id)
        {
            return db.InsuranceCategories.Find(id);
        }

        public List<Insurance> listInsurance(int id)
        {
            List<Insurance> result = db.Insurances.Where(x => x.InsuranceCategoryID == id && x.Status == 1).ToList();

            return result;
        }

        public List<PolicyInsurance> listRight(int id)
        {
            var result = (from po in db.PolicyInsurances
                          join ins in db.Insurances on po.InsuranceID equals ins.ID
                          where ins.InsuranceCategoryID == id
                          && po.PolicyInsuranceCategoryID == 1 && po.Status == 1
                          select po).ToList();
            return result;
        }

        public List<PolicyInsurance> listAdvantage(int id)
        {
            var result = (from po in db.PolicyInsurances
                          join ins in db.Insurances on po.InsuranceID equals ins.ID
                          where ins.InsuranceCategoryID == id
                          && po.PolicyInsuranceCategoryID == 2 && po.Status == 1
                          select po).ToList();
            return result;
        }

        public Insurance Find(int id)
        {
            return db.Insurances.Where(x => x.ID == id).SingleOrDefault();
        }

        public List<PolicyInsurance> listPolicy(int id, int insuranceID)
        {
            List<PolicyInsurance> result = db.PolicyInsurances.Where(x => x.InsuranceID == insuranceID && x.PolicyInsuranceCategoryID == id && x.Status == 1).ToList();
            return result;
        }

        public List<PolicyInsurance> listFile(int id)
        {
            var result = (from po in db.PolicyInsurances
                          join ins in db.Insurances on po.InsuranceID equals ins.ID
                          where ins.ID == id
                          && po.PolicyInsuranceCategoryID == 9 && po.Status == 1
                          select po).ToList();
            return result;
        }
    }
}
