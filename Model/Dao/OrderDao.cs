﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class OrderDao
    {
        DataModel db = null;

        public OrderDao()
        {
            db = new DataModel();
        }

        public int InsertAddress(Address address)
        {
            try
            {
                db.Addresses.Add(address);
                db.SaveChanges();
                return address.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int InsertContract(Contract contract)
        {
            try
            {
                db.Contracts.Add(contract);
                db.SaveChanges();
                return contract.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int InsertOrder(OrderDetail orderdetail)
        {
            try
            {
                db.OrderDetails.Add(orderdetail);
                db.SaveChanges();
                return orderdetail.ContractID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public Contract FindContract(int id)
        {
            return db.Contracts.Find(id);
        }

        public OrderDetail FindOrder(int id)
        {
            var result = (from i in db.Contracts
                          join o in db.OrderDetails
   on i.ID equals o.ContractID
                          where i.ID == id
                          select o).FirstOrDefault();
            return result;
        }

        public Address FindAddress(int id)
        {
            return db.Addresses.Find(id);
        }

        public Provind FindProvind(int id)
        {
            return db.Provinds.Find(id);
        }

        public District FindDistrict(int id)
        {
            return db.Districts.Find(id);
        }

        public Ward FindWard(int id)
        {
            return db.Wards.Find(id);
        }

        public bool UpdateContract(int id)
        {
            try
            {
                var result = db.Contracts.Find(id);
                result.Status = 2;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public PayMent Payment(PayMent pay)
        {
            try
            {
                db.PayMents.Add(pay);
                db.SaveChanges();
                return pay;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
