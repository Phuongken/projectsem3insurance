﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.Dao
{
    public class PolicyInsuranceCategoryDao
    {
        DataModel db = null;
        public PolicyInsuranceCategoryDao()
        {
            db = new DataModel();
        }
        public bool Insert(PolicyInsuranceCategory policyInsuranceCategory)
        {
            try
            {
                db.PolicyInsuranceCategories.Add(policyInsuranceCategory);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<PolicyInsuranceCategory> Index()
        {
            List<PolicyInsuranceCategory> result = (from s in db.PolicyInsuranceCategories where s.Status == 1 select s).ToList();
            return result;
        }
        public PolicyInsuranceCategory find(int? id)
        {
            return db.PolicyInsuranceCategories.Find(id);
        }
        public bool Update(PolicyInsuranceCategory newPolicyInsuranceCategory)
        {
            try
            {
                var policyInsuranceCategory = db.PolicyInsuranceCategories.Find(newPolicyInsuranceCategory.ID);
                policyInsuranceCategory.Name = newPolicyInsuranceCategory.Name;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(int? id)
        {
            try
            {
                var policyInsuranceCategory = db.PolicyInsuranceCategories.Find(id);
                policyInsuranceCategory.Status = 0;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
