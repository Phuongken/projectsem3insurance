﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.Dao
{
    public class InsuranceCategoryDao
    {
        DataModel db = null;
        public InsuranceCategoryDao()
        {
            db = new DataModel();
        }
        public bool Insert(InsuranceCategory insuranceCategory)
        {
            try
            {
                db.InsuranceCategories.Add(insuranceCategory);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<Insurance> Index(int id)
        {
            List<Insurance> result = (from s in db.Insurances where s.Status == 1 && s.InsuranceCategoryID == id select s).ToList();
            return result;
        }
        public List<InsuranceCategory> viewbagIndex()
        {
            List<InsuranceCategory> result = (from s in db.InsuranceCategories where s.Status == 1 select s).ToList();
            return result;
        }
        public InsuranceCategory find(int? id)
        {
            return db.InsuranceCategories.Find(id);
        }
        public bool Update(InsuranceCategory newInsuranceCategory)
        {
            try
            {
                var insuranceCategory = db.InsuranceCategories.Find(newInsuranceCategory.ID);
                insuranceCategory.Name = newInsuranceCategory.Name;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(int? id)
        {
            try
            {
                var insuranceCategory = db.InsuranceCategories.Find(id);
                insuranceCategory.Status = 0;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
