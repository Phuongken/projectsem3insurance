﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.Dao
{
    public class ContractDao
    {
        DataModel db = null;
        public ContractDao()
        {
            db = new DataModel();
        }
        public List<Contract> Index()
        {
            List<Contract> result = (from c in db.Contracts select c).ToList();
            return result;
        }
        public Contract Find(int? id)
        {
            return db.Contracts.Find(id);
        }
        public bool Update(Contract newContract, int adminID)
        {
            try
            {
                var contract = db.Contracts.Find(newContract.ID);
                contract.ContractName = newContract.ContractName;
                contract.StartDate = newContract.StartDate;
                contract.EndDate = newContract.EndDate;
                contract.PaymentAddress = newContract.PaymentAddress;
                contract.DeliveryAddress = newContract.DeliveryAddress;
                contract.Status = newContract.Status;
                contract.UpdateBy = adminID;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateOrderDetail(OrderDetail orderDetail)
        {
            try
            {
                var model = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == orderDetail.ContractID select o).FirstOrDefault();

                model.Quantity = orderDetail.Quantity;
                model.Price = orderDetail.Price;
                model.StartDate = orderDetail.StartDate;
                model.EndDate = orderDetail.EndDate;
                model.FullName = orderDetail.FullName;
                model.HomePhone = orderDetail.HomePhone;
                model.MobilePhone = orderDetail.MobilePhone;
                model.Email = orderDetail.Email;
                model.AddressDetail = orderDetail.AddressDetail;
                model.AddressContract = orderDetail.AddressContract;
                model.FullNameReceive = orderDetail.FullNameReceive;
                model.Identity = orderDetail.Identity;
                model.Dob = orderDetail.Dob;
                model.Gender = orderDetail.Gender;
                model.Relationship = orderDetail.Relationship;
                model.Job = orderDetail.Job;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        public bool Delete(int? id, int adminID)
        {
            try
            {
                var contract = db.Contracts.Find(id);
                contract.Status = 0;
                contract.UpdateBy = adminID;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<OrderDetail> getOrderDetails(int id)
        {
            List<OrderDetail> result = (from o in db.OrderDetails join c in db.Contracts on o.ContractID equals c.ID where c.ID == id select o).ToList();
            return result;
        }
        public List<OrderDetail> getOrderDetail()
        {
            List<OrderDetail> result = (from c in db.OrderDetails select c).ToList();
            return result;
        }
        public List<InfoOnline> getInfoOnline()
        {
            List<InfoOnline> result = db.InfoOnlines.Where(x => x.Status == 1).ToList();
            return result;
        }
        public List<User> getInfoUser()
        {
            List<User> result = (from c in db.Users where c.Status == 1 select c).ToList();
            return result;
        }
        public string getOrderDetail(int id)
        {
            var result = (from o in db.OrderDetails join c in db.Contracts on o.ContractID equals c.ID where c.ID == id select o.Email).FirstOrDefault();
            return result;
        }
        //
        
        public bool InsertHistoryContract(int id)
        {
            try
            {
                var result = Find(id);

                HistoryContract history = new HistoryContract()
                {
                    ContractID = result.ID,
                    UserID = result.UserID,
                    ContractName = result.ContractName,
                    TotalPrice = result.TotalPrice,
                    StartDate = result.StartDate,
                    EndDate = result.EndDate,
                    PaymentAddress = result.PaymentAddress,
                    DeliveryAddress = result.DeliveryAddress,
                    CreateDate = result.CreateDate,
                    UpdateDate = DateTime.Now,
                    CreateBy = result.CreateBy,
                    UpdateBy = result.UpdateBy,
                    Status = result.Status
                };

                var result1 = (from c in db.Contracts
                               join o in db.OrderDetails
        on c.ID equals o.ContractID
                               where c.ID == id
                               select o).FirstOrDefault();

                HistoryOrderDetail detail = new HistoryOrderDetail()
                {
                    InsurantID = result1.InsurantID,
                    ContractID = result1.ContractID,
                    Quantity = result1.Quantity,
                    Price = result1.Price,
                    StartDate = result1.StartDate,
                    EndDate = result1.EndDate,
                    FullName = result1.FullName,
                    HomePhone = result1.HomePhone,
                    MobilePhone = result1.MobilePhone,
                    Email = result1.Email,
                    AddressDetail = result1.AddressDetail,
                    AddressID = result1.AddressID,
                    AddressContract = result1.AddressContract,
                    AddressIDContract = result1.AddressIDContract,
                    FullNameReceive = result1.FullNameReceive,
                    Dob = result1.Dob,
                    Identity = result1.Identity,
                    Relationship = result1.Relationship,
                    Gender = result1.Gender,
                    Job = result1.Job,
                    TermInsurance = result1.TermInsurance,
                    HousePrice = result1.HousePrice
                };
                db.HistoryContracts.Add(history);
                db.HistoryOrderDetails.Add(detail);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //
        public bool ChangeStatusDone(int id, int adminID)
        {
            try
            {
                var contract = db.Contracts.Find(id);
                if (contract.Status == 2)
                {
                    contract.UpdateBy = adminID;
                    contract.Status = 1;                    
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //
        public bool ChangeStatusOpen(int id,int adminID)
        {
            try
            {
                var contract = db.Contracts.Find(id);
                if (contract.Status == 0)
                {
                    contract.UpdateBy = adminID;
                    contract.Status = 1;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
