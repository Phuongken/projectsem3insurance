﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class InfoOnlineDao
    {
        DataModel db = null;

        public InfoOnlineDao()
        {
            db = new DataModel();
        }

        public List<InfoOnline> ListInfoOnline()
        {
            List<InfoOnline> result = db.InfoOnlines.Where(x => x.Status == 1).ToList();
            return result;
        }
        public bool Delete(int? id)
        {
            try
            {
                var delete = db.InfoOnlines.Find(id);
                delete.Status = 0;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }

}
