﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class PaymentDao
    {
        DataModel db = null;
        public PaymentDao()
        {
            db = new DataModel();
        }
        public List<PayMent> ListPayment()
        {
            List<PayMent> result = db.PayMents.ToList();
            return result;

        }
        public bool changeStatusDone(int? id)
        {
            try
            {
                var status = db.PayMents.Find(id);
                var result = status.PaymentStatus;
                if (result == "Pending")
                {
                    status.PaymentStatus = "Done";
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }

        }
        public bool changeStatusCancel(int? id)
        {
            try
            {
                var status = db.PayMents.Find(id);
                var result = status.PaymentStatus;
                if (result == "Pending")
                {
                    status.PaymentStatus = "Cancel";
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }

        }
    }
}
