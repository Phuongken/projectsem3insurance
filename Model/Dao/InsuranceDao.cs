﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.Dao
{
    public class InsuranceDao
    {
        DataModel db = null;
        public InsuranceDao()
        {
            db = new DataModel();
        }
        public bool Insert(Insurance insurance)
        {
            try
            {
                db.Insurances.Add(insurance);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<Insurance> Index()
        {
            List<Insurance> result = (from s in db.Insurances where s.Status == 1 select s).ToList();
            return result;
        }
        public Insurance find(int? id)
        {
            return db.Insurances.Find(id);
        }
        public bool Update(Insurance newInsurance)
        {
            try
            {
                var insurance = db.Insurances.Find(newInsurance.ID);
                insurance.Name = newInsurance.Name;
                insurance.Avatar = newInsurance.Avatar;
                insurance.Price = newInsurance.Price;
                insurance.LimitPrice = newInsurance.LimitPrice;
                insurance.MinTerm = newInsurance.MinTerm;
                insurance.MaxTerm = newInsurance.MaxTerm;
                insurance.FeeInsurance = newInsurance.FeeInsurance;
                insurance.UpdateDate = DateTime.Now;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(int? id)
        {
            try
            {
                var insurance = db.Insurances.Find(id);
                insurance.Status = 0;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
