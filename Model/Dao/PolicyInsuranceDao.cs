﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.Dao
{
    public class PolicyInsuranceDao
    {
        DataModel db = null;
        public PolicyInsuranceDao()
        {
            db = new DataModel();
        }
        public bool Insert(PolicyInsurance policyInsurance, int id)
        {
            try
            {
                if (id != policyInsurance.InsuranceID)
                {
                    return false;
                }
                else
                {
                    policyInsurance.InsuranceID = id;
                    db.PolicyInsurances.Add(policyInsurance);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<PolicyInsurance> Index(int? id)
        {
            List<PolicyInsurance> result = (from s in db.PolicyInsurances where s.Status == 1 && s.InsuranceID == id select s).ToList();
            return result;
        }
        public List<PolicyInsurance> QuyenLoiChinh(int? id)
        {
            List<PolicyInsurance> result = (from s in db.PolicyInsurances where s.Status == 1 && s.InsuranceID == id && s.PolicyInsuranceCategoryID == 2 select s).ToList();
            return result;
        }
        public List<PolicyInsurance> UuDiem(int? id)
        {
            List<PolicyInsurance> result = (from s in db.PolicyInsurances where s.Status == 1 && s.InsuranceID == id && s.PolicyInsuranceCategoryID == 3 select s).ToList();
            return result;
        }
        public List<PolicyInsurance> ThoiGianCho(int? id)
        {
            List<PolicyInsurance> result = (from s in db.PolicyInsurances where s.Status == 1 && s.InsuranceID == id && s.PolicyInsuranceCategoryID == 4 select s).ToList();
            return result;
        }
        public List<PolicyInsurance> QuyDinhDongChiTra(int? id)
        {
            List<PolicyInsurance> result = (from s in db.PolicyInsurances where s.Status == 1 && s.InsuranceID == id && s.PolicyInsuranceCategoryID == 5 select s).ToList();
            return result;
        }
        public List<PolicyInsurance> LoaiTru(int? id)
        {
            List<PolicyInsurance> result = (from s in db.PolicyInsurances where s.Status == 1 && s.InsuranceID == id && s.PolicyInsuranceCategoryID == 6 select s).ToList();
            return result;
        }
        public List<PolicyInsurance> DoiTuongThamGia(int? id)
        {
            List<PolicyInsurance> result = (from s in db.PolicyInsurances where s.Status == 1 && s.InsuranceID == id && s.PolicyInsuranceCategoryID == 7 select s).ToList();
            return result;
        }
        public List<PolicyInsurance> QuyTac(int? id)
        {
            List<PolicyInsurance> result = (from s in db.PolicyInsurances where s.Status == 1 && s.InsuranceID == id && s.PolicyInsuranceCategoryID == 8 select s).ToList();
            return result;
        }
        public List<PolicyInsurance> ChuongTrinh(int? id)
        {
            List<PolicyInsurance> result = (from s in db.PolicyInsurances where s.Status == 1 && s.InsuranceID == id && s.PolicyInsuranceCategoryID == 9 select s).ToList();
            return result;
        }
        public PolicyInsurance find(int? id)
        {
            return db.PolicyInsurances.Find(id);
        }
        public bool Update(PolicyInsurance newPolicyInsurance)
        {
            try
            {
                var policyInsurance = db.PolicyInsurances.Find(newPolicyInsurance.ID);
                policyInsurance.Content = newPolicyInsurance.Content;
                policyInsurance.PolicyInsuranceCategoryID = newPolicyInsurance.PolicyInsuranceCategoryID;
                policyInsurance.UpdateDate = DateTime.Now;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(int? id)
        {
            try
            {
                var policyInsurance = db.PolicyInsurances.Find(id);
                policyInsurance.Status = 0;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
