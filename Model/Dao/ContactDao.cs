﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class ContactDao
    {
        DataModel db = null;

        public ContactDao()
        {
            db = new DataModel();
        }
        public List<Contact> ListContact()
        {
            List<Contact> result = db.Contacts.Where(x => x.Status == 1).ToList();
            return result;
        }
        public bool Insert(Contact contact)
        {
            try
            {
                db.Contacts.Add(contact);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InfoOnline(InfoOnline infoOnline)
        {
            try
            {
                db.InfoOnlines.Add(infoOnline);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Delete(int? id)
        {
            try
            {
                var delete = db.Contacts.Find(id);
                delete.Status = 0;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

