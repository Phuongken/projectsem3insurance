﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class UserDao
    {
        DataModel db = null;

        public UserDao()
        {
            db = new DataModel();
        }

        public User find(int? id)
        {
            return db.Users.Find(id);
        }


        public int register(User entity)
        {
            try
            {
                if (db.Users.Any(x => x.Username == entity.Username))
                {
                    return -1;
                }
                string salt = Guid.NewGuid().ToString().Substring(0, 7);
                string pass = entity.Password;

                string MD5Hash = Encryptor.MD5Hash(pass + salt);


                entity.Password = MD5Hash;
                entity.Salt = salt;
                entity.Status = 1;
                entity.Dob = DateTime.Now;
                entity.CreateDate = DateTime.Now;
                entity.UpdateDate = DateTime.Now;
                entity.CreateBy = 0;
                entity.UpdateBy = 0;
                entity.Gender = 0;

                db.Users.Add(entity);
                db.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public object Register(User user)
        {
            throw new NotImplementedException();
        }

        public bool CheckUserName(string userName)
        {
            return db.Users.Count(x => x.Username == userName) > 0;
        }
        public bool CheckEmail(string email)
        {
            return db.Users.Count(x => x.Email == email) > 0;
        }
        public User GetById(string userName)
        {
            return db.Users.SingleOrDefault(x => x.Username == userName);
        }

        public int Login(string userName, string PassWord)
        {
            var result = db.Users.SingleOrDefault(x => x.Username == userName);
            if (result == null)
            {
                return -1;
            }
            else
            {
                if (result.Status != 1)
                {
                    return -1;
                }
                else
                {
                    string salt = result.Salt.ToString();
                    var MD5Hash = Encryptor.MD5Hash(PassWord + salt);
                    if (result.Password == MD5Hash)
                        return 1;
                    else
                        return -1;
                }
            }
        }



        public object GetById(object username)
        {
            throw new NotImplementedException();
        }

        public List<User> ListUser()
        {
            List<User> result = db.Users.Where(x => x.Status == 1).ToList();
            return result;
        }

        public bool Edit(User user)
        {
            try
            {
                var editUser = db.Users.Find(user.ID);
                editUser.Name = user.Name;
                editUser.Phone = user.Phone;
                editUser.Email = user.Email;
                editUser.Address = user.Address;
                editUser.Status = 1;
                editUser.UpdateDate = DateTime.Now;
                editUser.UpdateBy = user.UpdateBy;

                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
        }

        public bool Delete(int? id)
        {
            try
            {
                var delete = db.Users.Find(id);
                delete.Status = 0;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Setting( string userName, string passWord, string newPassword, string email)
        {
            try
            {
                var result1 = db.Users.SingleOrDefault(x => x.Username == userName);
                if (Encryptor.MD5Hash(passWord + result1.Salt) == result1.Password)
                {
                    result1.Password = Encryptor.MD5Hash(newPassword + result1.Salt);
                    //result1.Name = name;
                    result1.Email = email;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}



