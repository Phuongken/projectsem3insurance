﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.Dao
{
    public class NewCategoryDao
    {
        DataModel db = null;
        public NewCategoryDao()
        {
            db = new DataModel();
        }
        public bool Insert(CategoryNew categoryNew)
        {
            try
            {
                db.CategoryNews.Add(categoryNew);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<New> Index(int id)
        {
            List<New> result = (from s in db.News where s.Status == 1 && s.CategoryID == id select s).ToList();
            return result;
        }
        public List<CategoryNew> ViewBagNew()
        {
            List<CategoryNew> result = (from s in db.CategoryNews where s.Status == 1 select s).ToList();
            return result;
        }
        public CategoryNew find(int? id)
        {
            return db.CategoryNews.Find(id);
        }
        public bool Update(CategoryNew newCategoryNew)
        {
            try
            {
                var categoryNew = db.CategoryNews.Find(newCategoryNew.ID);
                categoryNew.Name = newCategoryNew.Name;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(int? id)
        {
            try
            {
                var categoryNew = db.CategoryNews.Find(id);
                categoryNew.Status = 0;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
