﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class EmployeeDao
    {
        DataModel db = null;
        public EmployeeDao()
        {
            db = new DataModel();
        }
        public bool Register(Employee emp)
        {
            try
            {
                if(db.Employees.Any(x=>x.Username == emp.Username))
                {
                    return false;
                }
                else
                {
                    string salt = Guid.NewGuid().ToString().Substring(0, 7);
                    string pass = emp.Password;

                    string MD5Hash = Encryptor.MD5Hash(pass + salt);

                    emp.Password = MD5Hash;
                    emp.ConfirmPassword = MD5Hash;
                    emp.Salt = salt;
                    emp.UpdateDate = DateTime.Now;
                    emp.Status = 1;
                    emp.CreateDate = DateTime.Now;

                    db.Employees.Add(emp);
                    db.SaveChanges();
                    return true;

                }
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        public Employee GetById(string userName)
        {
            return db.Employees.SingleOrDefault(x => x.Username == userName);
        }
        public bool Login(string userName, string passWord)
        {
            var result = db.Employees.SingleOrDefault(x => x.Username == userName);
            if (result == null)
            {
                return false;
            }
            else
            {
                if (result.Status == 0)
                {
                    return false;
                }
                else
                {
                    string salt = result.Salt.ToString();
                    string MD5Hash = Encryptor.MD5Hash(passWord + salt);
                    if (result.Password == MD5Hash)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
}
