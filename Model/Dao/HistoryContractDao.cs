﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class HistoryContractDao
    {
        DataModel db = null;
        public HistoryContractDao()
        {
            db = new DataModel();
        }
        public Contract findId(int? id)
        {
            return db.Contracts.Find(id);
        }

        public List<HistoryContract> GetHistoryContract(int id)
        {
            List<HistoryContract> contract = db.HistoryContracts.Where(x => x.UserID == id).ToList();
            return contract;
        }
        public List<OrderDetail> GetOrderDetail(int id)
        {
            List<OrderDetail> orderDetail = db.OrderDetails.Where(x => x.InsurantID == id).ToList();
            return orderDetail;
        }
    }
}
