﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class ChartDao
    {
        DataModel db = null;

        public ChartDao()
        {
            db = new DataModel();
        }

        public List<Insurance> ListInsurance()
        {
            List<Insurance> list = db.Insurances.ToList();
            return list;
        }

        public int CountOrderID(int id, string startDate, string endDate)
        {
            var StartDate = Convert.ToDateTime(startDate);
            var EndDate = Convert.ToDateTime(endDate);
            var result = from c in db.Contracts
                         join o in db.OrderDetails on c.ID equals o.ContractID
                         where o.InsurantID == id && c.UpdateDate <= EndDate && c.UpdateDate >= StartDate
                         select c;
            return result.Count();
        }

        public int CountOrderIDOn(int id, string startDate, string endDate)
        {
            var StartDate = Convert.ToDateTime(startDate);
            var EndDate = Convert.ToDateTime(endDate);
            var result = from c in db.Contracts
                         join o in db.OrderDetails on c.ID equals o.ContractID
                         where o.InsurantID == id && c.Status == 1 && c.UpdateDate <= EndDate
                         && c.UpdateDate >= StartDate
                         select c;
            return result.Count();
        }
    }
}
