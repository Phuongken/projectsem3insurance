﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.Dao
{
    public class NewDao
    {
        DataModel db = null;
        public NewDao()
        {
            db = new DataModel();
        }
        public bool Insert(New new1)
        {
            try
            {
                db.News.Add(new1);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<New> Index(int id)
        {
            List<New> result = (from s in db.News where s.Status == 1 && s.CategoryID == id select s).ToList();
            return result;
        }
        public New find(int? id)
        {
            return db.News.Find(id);
        }
        public bool Update(New newNew1)
        {
            try
            {
                var new1 = db.News.Find(newNew1.ID);
                new1.Name = newNew1.Name;
                new1.Title = newNew1.Title;
                new1.Content = newNew1.Content;
                new1.Image = newNew1.Image;
                new1.UpdateDate = DateTime.Now;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(int? id)
        {
            try
            {
                var new1 = db.News.Find(id);
                new1.Status = 0;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<InfoOnline> getInfoOnline()
        {
            List<InfoOnline> result = db.InfoOnlines.Where(x => x.Status == 1).ToList();
            return result;
        }

        public New Find(int id)
        {
            return db.News.Find(id);
        }

        public List<New> getListNew()
        {
            List<New> list = db.News.OrderByDescending(x=> x.CreateDate).Take<New>(4).ToList();
            return list;
        }

        public Admin FindAdmin(int id)
        {
            return db.Admins.Find(id);
        }

        public Employee FindEmployee(int id)
        {
            return db.Employees.Find(id);
        }

        public List<New> ListNews(int id)
        {
            List<New> result = db.News.Where(x => x.CategoryID == id).ToList();
            return result;
        }
    }
}
