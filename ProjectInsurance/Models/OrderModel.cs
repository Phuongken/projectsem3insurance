﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectInsurance.Models
{
    [Serializable]
    public class OrderModel
    {
        public int InsurantID { get; set; }
        public int ContractID { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        [Required(ErrorMessage = "Ngày bắt đầu không được bỏ chống")]
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        [StringLength(50)]
        public string FullName { get; set; }
        public int HomePhone { get; set; }
        [Required(ErrorMessage = "Số điện thoại không được bỏ trống")]
        [DataType(DataType.PhoneNumber)]
        public int MobilePhone { get; set; }
        [Required(ErrorMessage = "Email không được bỏ trống")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Địa chỉ chi tiết không được bỏ trống")]
        [StringLength(50)]
        public string AddressDetail { get; set; }
        [Required(ErrorMessage = "Tên tỉnh thành phố không được bỏ trống")]
        public int ProvindRequest { get; set; }
        [Required(ErrorMessage = "Tên quận huyện không được bỏ trống")]
        public int DistrictRequest { get; set; }
        [Required(ErrorMessage = "Tên phường xã không được bỏ trống")]
        public int WardRequest { get; set; }
        [StringLength(50)]
        [Required(ErrorMessage = "Địa chỉ hợp đồng không được bỏ trống")]
        public string AddressContract { get; set; }
        [Required(ErrorMessage = "Tên tỉnh thành phố không được bỏ trống")]
        public int ProvindContract { get; set; }
        [Required(ErrorMessage = "Tên quận huyện không được bỏ trống")]
        public int DistrictContract { get; set; }
        [Required(ErrorMessage = "Tên phường xã không được bỏ trống")]
        public int WardContract { get; set; }
        [StringLength(50)]
        [Required(ErrorMessage = "Tên người nhận không được bỏ trống")]
        public string FullNameReceive { get; set; }
        [Required(ErrorMessage = "Chứng minh thư nhân dân không được bỏ trống")]
        [DataType(DataType.PhoneNumber)]
        public long Identity { get; set; }
        [Required(ErrorMessage = "Ngày sinh không được bỏ trống")]
        public DateTime? Dob { get; set; }
        [Required(ErrorMessage = "Quan hệ không được bỏ trống")]
        [DataType(DataType.PhoneNumber)]
        public int Relationship { get; set; }
        [Required(ErrorMessage = "Giới tính không được bỏ trống")]
        [DataType(DataType.PhoneNumber)]
        public int Gender { get; set; }
        [Required(ErrorMessage = "Công việc không được bỏ trống")]
        [StringLength(50)]
        public string Job { get; set; }
        public int TermInsurance { get; set; }
        public decimal? HousePrice { get; set; }
    }
}