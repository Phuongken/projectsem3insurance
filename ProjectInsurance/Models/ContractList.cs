﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectInsurance.Models
{
    public class ContractList
    {
        public string Name { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string address { get; set; }
        public int InsuranceID { get; set; }
        public int contractID { get; set; }
        public int status { get; set; }
        public string updateDate { get; set; }
    }
}