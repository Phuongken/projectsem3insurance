﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace ProjectInsurance.Models
{
    public class GetDataPaypal
    {
        public string GetPaypalResponse(string tx)
        {
            try
            {
                string authToken = WebConfigurationManager.AppSettings["Token"];
                string txtToken = tx;
                string query = string.Format("cmd=_notify-synch&tx={0}&at={1}", txtToken, authToken);
                string url = WebConfigurationManager.AppSettings["urlSubmitPayment"];
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = query.Length;
                StreamWriter outStreamWriter = new StreamWriter(req.GetRequestStream());
                outStreamWriter.Write(query);
                outStreamWriter.Close();
                StreamReader reader = new StreamReader(req.GetResponse().GetResponseStream(), Encoding.ASCII);
                string strResponse = reader.ReadToEnd();
                reader.Close();


                if (strResponse.StartsWith("SUCCESS"))
                    return strResponse;
                else
                    return String.Empty;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public PayMent InfomationPayment(string data)
        {
            string key, value;
            var payment = new PayMent();
            try
            {

                string[] strArray = data.Split('\n');
                for (int i = 1; i < strArray.Length; i++)
                {
                    string[] strArrayTemp = strArray[i].Split('=');
                    key = strArrayTemp[0];
                    value = HttpUtility.UrlDecode(strArrayTemp[1]);
                    switch (key)
                    {
                        case "mc_gross":
                            payment.GrossTotal = float.Parse(value);
                            break;
                        case "num_cart_items":
                            payment.InvoiceNumber = int.Parse(value);
                            break;
                        case "payment_status":
                            payment.PaymentStatus = value;
                            break;
                        case "first_name":
                            payment.PayerFirstName = value;
                            break;
                        case "mc_fee":
                            payment.PaymentFee = float.Parse(value);
                            break;
                        case "business":
                            payment.BusinessEmail = value;
                            break;
                        case "payer_email":
                            payment.PayerEmail = value;
                            break;
                        case "last_name":
                            payment.PayerLastName = value;
                            break;
                        case "receiver_email":
                            payment.ReceiverEmail = value;
                            break;
                        case "item_name1":
                            payment.ItemName = value;
                            break;
                        case "mc_currency":
                            payment.Currency = value;
                            break;
                        case "txn_id":
                            payment.TransactionId = value;
                            break;
                        case "payer_status":
                            payment.PayerStatus = value;
                            break;
                        case "item_number1":
                            payment.ItemNumber = value;
                            break;
                        case "payment_date":
                            payment.PaymentDate = value;
                            break;
                        case "payer_id":
                            payment.PayerID = value;
                            break;
                        case "receiver_id":
                            payment.ReceiverId = value;
                            break;
                    }
                }
                return payment;
            }
            catch (Exception ex)
            {
                return payment;
            }
        }

    }
}