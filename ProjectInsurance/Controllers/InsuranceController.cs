﻿using Model.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Controllers
{
    public class InsuranceController : Controller
    {

        ClientDao dao = new ClientDao();

        // GET: Insurance
        public ActionResult Index(int id)
        {
            Session["LoginRedirect"] = "/baohiem/" + id;
            var list = dao.listInsurance(id);


            ViewBag.NameCategory = dao.NameCategory(id).Name;
            ViewBag.listRight = dao.listRight(id);
            ViewBag.listAdvantage = dao.listAdvantage(id);

            return View(list);
            
        }


        public ActionResult Detail(int id)
        {
            Session["LoginRedirect"] = "/chi-tiet-bao-hiem" + id;
            PolicyInsuranceCategoryDao dao1 = new PolicyInsuranceCategoryDao();
            ViewBag.policyCategory = dao1.Index();
            return View(dao.Find(id));
        }


        public JsonResult policy(string id, string insuranceID)
        {
            var list = dao.listPolicy(Convert.ToInt32(id), Convert.ToInt32(insuranceID));

            return Json(new
            {
                value = list,
                JsonRequestBehavior.AllowGet
            });
        }
    }
}