﻿using Model.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Controllers
{
    public class NewsController : Controller
    {

        public ActionResult Index(int id)
        {
            NewDao dao = new NewDao();
            var result = dao.Find(id);

            ViewBag.idAdmin = dao.FindAdmin(result.CreateBy);

            ViewBag.idEmployee = dao.FindAdmin(result.CreateBy);

            return View(result);
        }


        public ActionResult News(int id)
        {
            NewDao dao = new NewDao();
            
            return View(dao.ListNews(id));
        }

    }
}