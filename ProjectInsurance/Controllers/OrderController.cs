﻿using Model.Dao;
using Model.EF;
using ProjectInsurance.Common;
using ProjectInsurance.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Controllers
{
    public class OrderController : Controller
    {

        ClientDao dao = new ClientDao();
        DataModel db = new DataModel();
        InsuranceDao inDao = new InsuranceDao();

        [HttpGet]
        public ActionResult Index(int id)
        {
            Session["LoginRedirect"] = "/dang-ky-bao-hiem/" + id;
           var order = (OrderModel)Session["OrderSession"];

            var checkInsurance = inDao.find(id);

            if (checkInsurance.InsuranceCategoryID == 1)
            {
                Session["Baohiemnhantho"] = 1;
                Session["Baohiemyte"] = null;
                Session["Baohiemxenmay"] = null;
                Session["Baohiemnhao"] = null;
            }
            else if (checkInsurance.InsuranceCategoryID == 2)
            {
                Session["Baohiemnhantho"] = null;
                Session["Baohiemyte"] = 1;
                Session["Baohiemxenmay"] = null;
                Session["Baohiemnhao"] = null;
            }
            else if (checkInsurance.InsuranceCategoryID == 3)
            {
                Session["Baohiemnhantho"] = null;
                Session["Baohiemyte"] = null;
                Session["Baohiemxenmay"] = 1;
                Session["Baohiemnhao"] = null;
            }
            else if (checkInsurance.InsuranceCategoryID == 4)
            {
                Session["Baohiemnhantho"] = null;
                Session["Baohiemyte"] = null;
                Session["Baohiemxenmay"] = null;
                Session["Baohiemnhao"] = 1;
            }

            if (order != null)
            {
                ViewBag.listRight = dao.listPolicy(1, order.InsurantID);
                ViewBag.ProvindRequest = db.Provinds.Where(x => x.ID == order.ProvindRequest).SingleOrDefault();
                ViewBag.ProvindContract = db.Provinds.Where(x => x.ID == order.ProvindContract).SingleOrDefault();
                ViewBag.DistrictRequest = db.Districts.Where(x => x.ID == order.DistrictRequest).SingleOrDefault();
                ViewBag.DistrictContract = db.Districts.Where(x => x.ID == order.DistrictContract).SingleOrDefault();
                ViewBag.WardRequest = db.Wards.Where(x => x.ID == order.WardRequest).SingleOrDefault();
                ViewBag.WardContract = db.Wards.Where(x => x.ID == order.WardContract).SingleOrDefault();
            }
            ViewBag.Provind = db.Provinds.ToList();
            ViewBag.ProvindA = "";
            ViewBag.District = "";
            ViewBag.Ward = "";
            ViewBag.Provind1 = "";
            ViewBag.District1 = "";
            ViewBag.Ward1 = "";
            ViewBag.Relationship = "";
            ViewBag.Gender = "";
            ViewBag.listRight = dao.listPolicy(1, id);
            ViewBag.Insurance = dao.Find(id);
            ViewBag.RelationshipCheck = 0;
            ViewBag.GenderCheck = 0;

            return View();
        }

        [HttpPost]
        public ActionResult Index(OrderModel model)
        {
            var order = (OrderModel)Session[CommonConstants.ORDER_SESSION];
            if (order != null)
            {
                ViewBag.Provind = db.Provinds.ToList();
                ViewBag.Insurance = dao.Find(model.InsurantID);
                ViewBag.listRight = dao.listPolicy(1, order.InsurantID);
                ViewBag.ProvindRequest = db.Provinds.Where(x => x.ID == order.ProvindRequest).SingleOrDefault();
                ViewBag.ProvindContract = db.Provinds.Where(x => x.ID == order.ProvindContract).SingleOrDefault();
                ViewBag.DistrictRequest = db.Districts.Where(x => x.ID == order.DistrictRequest).SingleOrDefault();
                ViewBag.DistrictContract = db.Districts.Where(x => x.ID == order.DistrictContract).SingleOrDefault();
                ViewBag.WardRequest = db.Wards.Where(x => x.ID == order.WardRequest).SingleOrDefault();
                ViewBag.WardContract = db.Wards.Where(x => x.ID == order.WardContract).SingleOrDefault();

            }
            ViewBag.Provind = db.Provinds.ToList();
            ViewBag.listRight = dao.listPolicy(1, model.InsurantID);
            ViewBag.Insurance = dao.Find(model.InsurantID);
            ViewBag.ProvindRequest = db.Provinds.Where(x => x.ID == model.ProvindRequest).SingleOrDefault();
            ViewBag.ProvindContract = db.Provinds.Where(x => x.ID == model.ProvindContract).SingleOrDefault();
            ViewBag.DistrictRequest = db.Districts.Where(x => x.ID == model.DistrictRequest).SingleOrDefault();
            ViewBag.DistrictContract = db.Districts.Where(x => x.ID == model.DistrictContract).SingleOrDefault();
            ViewBag.WardRequest = db.Wards.Where(x => x.ID == model.WardRequest).SingleOrDefault();
            ViewBag.WardContract = db.Wards.Where(x => x.ID == model.WardContract).SingleOrDefault();
            ViewBag.ProvindA = "";
            ViewBag.District = "";
            ViewBag.Ward = "";
            ViewBag.Provind1 = "";
            ViewBag.District1 = "";
            ViewBag.Ward1 = "";
            ViewBag.Relationship = "";
            ViewBag.Gender = "";
            ViewBag.RelationshipCheck = model.Relationship;
            ViewBag.GenderCheck = model.Gender;
            if (ModelState.IsValid)
            {
                InsuranceDao i = new InsuranceDao();
                var result = i.find(model.InsurantID);

                if (model.ProvindRequest == 0)
                {
                    ViewBag.ProvindA = "Tên tỉnh, thành phố không được bỏ trống";
                    return View(model);
                }
                else if (model.DistrictRequest == 0 && model.WardRequest == 0)
                {
                    ViewBag.District = "Tên quận, huyện không được bỏ trống";
                    ViewBag.Ward = "Tên xã, phường phố không được bỏ trống";
                    return View(model);
                }
                else if (model.WardRequest == 0)
                {
                    ViewBag.Ward = "Tên xã, phường phố không được bỏ trống";
                    return View(model);
                }

                if (model.ProvindContract == 0)
                {
                    ViewBag.Provind1 = "Tên tỉnh, thành phố không được bỏ trống";
                    return View(model);
                }
                else if (model.DistrictContract == 0 && model.WardContract == 0)
                {
                    ViewBag.District1 = "Tên quận, huyện không được bỏ trống";
                    ViewBag.Ward = "Tên xã, phường phố không được bỏ trống";
                    return View(model);
                }
                else if (model.WardContract == 0)
                {
                    ViewBag.Ward1 = "Tên xã, phường phố không được bỏ trống";
                    return View(model);
                }

                if (model.Relationship == 0)
                {
                    ViewBag.Relationship = "Quan hệ không được bỏ trống";
                    return View(model);
                }

                if (model.Gender == 0)
                {
                    ViewBag.Gender = "Giới tính không được bỏ trống";
                    return View(model);
                }

                if (result != null)
                {
                    var orderSession = new OrderModel();
                    orderSession.ContractID = 0;
                    orderSession.Quantity = 1;
                    orderSession.InsurantID = model.InsurantID;
                    orderSession.StartDate = model.StartDate;
                    orderSession.EndDate = model.EndDate;
                    orderSession.FullName = model.FullName;
                    orderSession.HomePhone = model.HomePhone;
                    orderSession.MobilePhone = model.MobilePhone;
                    orderSession.AddressDetail = model.AddressDetail;
                    orderSession.ProvindRequest = model.ProvindRequest;
                    orderSession.Price = model.Price;
                    orderSession.DistrictRequest = model.DistrictRequest;
                    orderSession.WardRequest = model.WardRequest;
                    orderSession.AddressContract = model.AddressContract;
                    orderSession.ProvindContract = model.ProvindContract;
                    orderSession.DistrictContract = model.DistrictContract;
                    orderSession.WardContract = model.WardContract;
                    orderSession.FullNameReceive = model.FullNameReceive;
                    orderSession.Dob = model.Dob;
                    orderSession.Identity = model.Identity;
                    orderSession.Relationship = model.Relationship;
                    orderSession.Job = model.Job;
                    orderSession.Email = model.Email;
                    orderSession.Gender = model.Gender;
                    orderSession.TermInsurance = model.TermInsurance;
                    orderSession.HousePrice = model.HousePrice;

                    Session["OrderSession"] = orderSession;
                    return RedirectToAction("Review", "Order");
                }

            }

            return View(model);
        }

        public ActionResult Review()
        {

            Session["LoginRedirect"] = "/xem-lai-dang-ky";
            var order = (OrderModel)Session["OrderSession"];
            if (order != null)
            {
                ViewBag.ListFile = dao.listFile(order.InsurantID); ;
                ViewBag.listRight = dao.listPolicy(1, order.InsurantID);
                ViewBag.ProvindRequest = db.Provinds.Where(x => x.ID == order.ProvindRequest).SingleOrDefault();
                ViewBag.ProvindContract = db.Provinds.Where(x => x.ID == order.ProvindContract).SingleOrDefault();
                ViewBag.DistrictRequest = db.Districts.Where(x => x.ID == order.DistrictRequest).SingleOrDefault();
                ViewBag.DistrictContract = db.Districts.Where(x => x.ID == order.DistrictContract).SingleOrDefault();
                ViewBag.WardRequest = db.Wards.Where(x => x.ID == order.WardRequest).SingleOrDefault();
                ViewBag.WardContract = db.Wards.Where(x => x.ID == order.WardContract).SingleOrDefault();

                return View(dao.Find(order.InsurantID));
            }
            return View("Index");
        }

        public JsonResult GetAllProvind()
        {
            var data = db.Provinds.ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllDistrictByProvind(int txtDistrict)
        {
            var data = db.Districts.Where(x => x.ProvindID == txtDistrict).OrderBy(x => x.Name).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllWardByDistrict(int txtWard)
        {
            var data = db.Wards.Where(x => x.DistrictID == txtWard).OrderBy(x => x.Name).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAddress(int provindID, int districtID, int wardID)
        {
            var data = db.Provinds.Where(x => x.ID == provindID).SingleOrDefault();
            var data1 = db.Districts.Where(x => x.ID == districtID).SingleOrDefault();
            var data2 = db.Wards.Where(x => x.ID == wardID).SingleOrDefault();

            return Json(new
            {
                provind = data,
                district = data1,
                ward = data2
            }, JsonRequestBehavior.AllowGet);
        }


    }
}