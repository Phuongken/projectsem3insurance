﻿using Model.Dao;
using Model.EF;
using ProjectInsurance.Common;
using ProjectInsurance.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace ProjectInsurance.Controllers
{
    public class CheckoutController : Controller
    {
        OrderDao dao = new OrderDao();
        ClientDao dao1 = new ClientDao();
        // GET: Checkout
        public ActionResult Index()
        {
            Session["LoginRedirect"] = "/thanh-toan-bao-hiem";

            var order = (OrderModel)Session["OrderSession"];


            if (Session["UserID"] != null)
            {
                if (order != null)
                {
                    var insunrace = dao1.Find(order.InsurantID);
                    Address addressInsert = new Address()
                    {
                        ProvindID = order.ProvindRequest,
                        DistrictID = order.DistrictRequest,
                        WardID = order.WardRequest
                    };

                    Address addressContractInsert = new Address()
                    {
                        ProvindID = order.ProvindContract,
                        DistrictID = order.DistrictContract,
                        WardID = order.WardContract
                    };

                    int AddressId = dao.InsertAddress(addressInsert);
                    int AddressContractID = dao.InsertAddress(addressContractInsert);

                    if (AddressId != 0 && AddressContractID != 0)
                    {
                        Contract contract = new Contract()
                        {
                            UserID = Convert.ToInt32(Session["UserID"]),
                            ContractName = insunrace.Name,
                            TotalPrice = order.Price,
                            StartDate = order.StartDate,
                            EndDate = order.EndDate,
                            PaymentAddress = order.AddressContract,
                            DeliveryAddress = order.AddressContract,
                            CreateDate = DateTime.Now,
                            UpdateDate = DateTime.Now,
                            CreateBy = Convert.ToInt32(Session["UserID"]),
                            UpdateBy = 0,
                            Status = 0
                        };

                        int ContractID = dao.InsertContract(contract);

                        Session["ContractID"] = ContractID;

                        if (ContractID != 0)
                        {
                            if (order.HousePrice != null)
                            {
                                OrderDetail detail = new OrderDetail()
                                {
                                    InsurantID = insunrace.ID,
                                    ContractID = ContractID,
                                    Quantity = order.Quantity,
                                    Price = order.Price,
                                    StartDate = order.StartDate,
                                    EndDate = order.EndDate,
                                    FullName = order.FullName,
                                    HomePhone = order.HomePhone,
                                    MobilePhone = order.MobilePhone,
                                    Email = order.Email,
                                    AddressDetail = order.AddressDetail,
                                    AddressID = AddressId,
                                    AddressContract = order.AddressContract,
                                    AddressIDContract = AddressContractID,
                                    FullNameReceive = order.FullNameReceive,
                                    Dob = order.Dob,
                                    Identity = order.Identity,
                                    Relationship = order.Relationship,
                                    Gender = order.Gender,
                                    Job = order.Job,
                                    TermInsurance = order.TermInsurance,
                                    HousePrice = order.HousePrice
                                };
                                int orderDetail = dao.InsertOrder(detail);

                                var usd = LoadVietcombank().Exrates[18].Transfer;

                                ViewBag.ExrateList = LoadVietcombank().Exrates;
                                ViewBag.TimeExrate = LoadVietcombank().DateTime;

                                ViewBag.ContractID = ContractID;
                                ViewBag.InsuranceName = insunrace.Name;
                                ViewBag.Price = order.Price;
                                ViewBag.PriceUSD = (order.Price) / Convert.ToInt32(usd);
                                ViewBag.Quantity = order.Quantity;
                                return View();
                            }
                            else
                            {
                                OrderDetail detail = new OrderDetail()
                                {
                                    InsurantID = insunrace.ID,
                                    ContractID = ContractID,
                                    Quantity = order.Quantity,
                                    Price = order.Price,
                                    StartDate = order.StartDate,
                                    EndDate = order.EndDate,
                                    FullName = order.FullName,
                                    HomePhone = order.HomePhone,
                                    MobilePhone = order.MobilePhone,
                                    Email = order.Email,
                                    AddressDetail = order.AddressDetail,
                                    AddressID = AddressId,
                                    AddressContract = order.AddressContract,
                                    AddressIDContract = AddressContractID,
                                    FullNameReceive = order.FullNameReceive,
                                    Dob = order.Dob,
                                    Identity = order.Identity,
                                    Relationship = order.Relationship,
                                    Gender = order.Gender,
                                    Job = order.Job,
                                    TermInsurance = order.TermInsurance,
                                    HousePrice = 0
                                };
                                int orderDetail = dao.InsertOrder(detail);

                                var usd = LoadVietcombank().Exrates[18].Transfer;

                                ViewBag.ExrateList = LoadVietcombank().Exrates;
                                ViewBag.TimeExrate = LoadVietcombank().DateTime;

                                ViewBag.ContractID = ContractID;
                                ViewBag.InsuranceName = insunrace.Name;
                                ViewBag.Price = order.Price;
                                ViewBag.PriceUSD = (order.Price) / Convert.ToInt32(usd);
                                ViewBag.Quantity = order.Quantity;
                                return View();
                            }
                        }
                    }
                }

            }
            else
            {
                return View();
            }

            return RedirectToAction("Review", "Order");
        }

        public ActionResult ViewMail()
        {
            return View();
        }

        public ActionResult GetdataPaypal()
        {

            var getData = new GetDataPaypal();
            var payment = getData.InfomationPayment(getData.GetPaypalResponse(Request.QueryString["tx"]));
            if (Session["OrderSession"] != null)
            {
                if (payment != null)
                {
                    var payResult = dao.Payment(payment);
                    ViewBag.Payment = payResult;
                    var id = Session["ContractID"].ToString();
                    var result = dao.UpdateContract(Convert.ToInt32(id));
                    var insurance = dao.FindContract(Convert.ToInt32(id));
                    var detail = dao.FindOrder(Convert.ToInt32(id));
                    var address1 = dao.FindAddress(detail.AddressID);
                    var address2 = dao.FindAddress(detail.AddressIDContract);
                    var provind1 = dao.FindProvind(address1.ProvindID);
                    var provind2 = dao.FindProvind(address2.ProvindID);
                    var district1 = dao.FindDistrict(address1.DistrictID);
                    var district2 = dao.FindDistrict(address2.DistrictID);
                    var ward1 = dao.FindWard(address1.WardID);
                    var ward2 = dao.FindWard(address2.WardID);

                    var addressDetail1 = detail.AddressDetail + ", " + ward1.Name + ", " + district1.Name
                        + ", " + provind1.Name;

                    var addressDetail2 = detail.AddressContract + ", " + ward2.Name + ", " + district2.Name
                       + ", " + provind2.Name;

                    string content = System.IO.File.ReadAllText(Server.MapPath("/Views/Checkout/ViewMail.cshtml"));
                    content = content.Replace("{{InsuranceID}}", insurance.ID.ToString());
                    content = content.Replace("{{UserID}}", insurance.UserID.ToString());
                    content = content.Replace("{{NameInsurance}}", insurance.ContractName);
                    content = content.Replace("{{TotalPrice}}", insurance.TotalPrice.ToString());
                    content = content.Replace("{{AddressDetail}}", addressDetail1);
                    content = content.Replace("{{AddressContract}}", addressDetail2);
                    content = content.Replace("{{StartDate}}", insurance.StartDate.ToString());
                    content = content.Replace("{{EndDate}}", insurance.EndDate.ToString());
                    content = content.Replace("{{EndDate}}", insurance.EndDate.ToString());
                    content = content.Replace("{{Status}}", insurance.Status.ToString());

                    content = content.Replace("{{TransactionID}}", payment.TransactionId);
                    content = content.Replace("{{SendAccount}}", payment.PayerEmail);
                    content = content.Replace("{{ReceiverAccount}}", payment.ReceiverEmail);
                    content = content.Replace("{{InsuranceName}}", payment.ItemName);
                    content = content.Replace("{{InsuranceIDPay}}", payment.ItemNumber);
                    content = content.Replace("{{FeeInsurance}}", payment.GrossTotal.ToString());
                    content = content.Replace("{{StatusPay}}", payment.PaymentStatus);

                    SendMail mail = new SendMail();

                    mail.SendEmail(detail.Email, "Notice of receipt of the contract.", content);
                    if (result == true)
                    {
                        Session["OrderSession"] = null;
                        return View(insurance);
                    }
                }
            }
            return View();
        }

        public ExrateList LoadVietcombank()
        {
            string siteContent = string.Empty;

            string url = "https://www.vietcombank.com.vn/exchangerates/ExrateXML.aspx";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            XmlSerializer serializer = new XmlSerializer(typeof(ExrateList));
            ExrateList exrateList = (ExrateList)serializer.Deserialize(responseStream);

            return exrateList;
        }

    }
}