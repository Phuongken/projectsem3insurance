﻿using Model.Dao;
using Model.EF;
using Newtonsoft.Json;
using ProjectInsurance.Common;
using ProjectInsurance.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Controllers
{
    public class HomeController : Controller
    {
        ContactDao dao = new ContactDao();

        DataModel db = new DataModel();
        public ActionResult Index()
        {
            Session["LoginRedirect"] = "/";
            Session["LoginUser"] = null;
            ViewBag.ListNew = db.News.Where(x => x.Status == 1).ToList();
            return View();
        }
        public ActionResult ViewMail()
        {
            return View();
        }

        public JsonResult ChangeSession()
        {
            Session["SendMailSuccsess"] = null;
            Session["SendMailError"] = null;
            return Json(new
            {
                status = true
            });
        }
        [HttpPost]
        public JsonResult Contact(string FirstName, string LastName, string Mobile, string Email, string Content)
        {


            var contact = new Contact()
            {
                FirstName = FirstName,
                LastName = LastName,
                Mobile = Convert.ToInt32(Mobile),
                Email = Email,
                Content = Content,
                CreateDate = DateTime.Now,
                Status = 1
            };


            var result = dao.Insert(contact);

            if (result == true)
            {

                string content = System.IO.File.ReadAllText(Server.MapPath("/Views/Home/ViewMail.cshtml"));
                content = content.Replace("{{FirstName}}", FirstName);
                content = content.Replace("{{LastName}}", LastName);
                content = content.Replace("{{Mobile}}", Mobile);
                content = content.Replace("{{Content}}", Content);

                SendMail mail = new SendMail();

                mail.SendEmail(Email, "Notice of information confirmation.", content);

                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }

        public JsonResult SendMail(string Email)
        {
            bool result = false;

            var info = new InfoOnline()
            {
                Email = Email,
                Status = 1
            };

            SendMail mail = new SendMail();

            var result1 = dao.InfoOnline(info);

            result = mail.SendEmail(Email, "Notice of registration confirmation.", "You have successfully registered to receive free online news from Insurance team.");

            if (result1 = true && result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }

        public ActionResult SuccessRegister()
        {
            return View();
        }

        public ActionResult ErrorRegister()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(User model)
        {
            if (ModelState.IsValid)
            {
                var dao = new UserDao();
                if (dao.CheckUserName(model.Username))
                {
                    ModelState.AddModelError("", "Tên đăng nhập đã tồn tại");
                }
                else if (dao.CheckEmail(model.Email))
                {
                    ModelState.AddModelError("", "Email đã tồn tại");
                }
                else
                {

                    var result = dao.register(model);

                    if (result > 0)
                    {
                        ViewBag.Success = "Đăng kí thành công";
                        return View("SuccessRegister");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Đăng kí không thành công");
                        return View("ErrorRegister");
                    }
                }

            }
            return View("Index");
        }



        // Login
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var dao = new UserDao();
                var result = dao.Login(model.UserName, model.PassWord);
                if (result == 1)
                {
                    var user = dao.GetById(model.UserName);
                    Session["UserID"] = user.ID;
                    Session["UserName"] = user.Username;
                    Session["Email"] = user.Email;
                    Session["Name"] = user.Name;
                    var url = Session["LoginRedirect"];
                    return Redirect(url.ToString());
                }
                if (result == -1)
                {
                    Session["LoginUser"] = 1;
                    return View("ErrorLogin");
                }
            }
            return View("Index");
        }
        //LogOut
        public ActionResult Logout()
        {
            Session["UserId"] = null;
            Session["UserName"] = null;

            return Redirect("/");
        }
        [HttpPost]
        public ActionResult Setting(SettingModel settingModel)
        {
            if (ModelState.IsValid)
            {
                var dao = new UserDao();
                var result1 = dao.Setting(settingModel.UserName, settingModel.Password, settingModel.NewPassword, settingModel.Email);
                if (result1 == true)
                {
                    return Redirect("/");
                }
                else
                {
                    return View("Index");
                }
            }
            return View("Index");
        }

        public ActionResult Contract()
        {
            var id = Session["UserID"];
            var idUser = Convert.ToInt32(id);
            var result = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID
                          join i in db.Insurances on o.InsurantID equals i.ID
                          where c.UserID == idUser
                          select new {Name = c.ContractName, startDate = c.StartDate, endDate = c.EndDate
                          , address = c.DeliveryAddress, InsuranceID = i.ID, ContractID = c.ID
                          , status = c.Status, updateDate = c.UpdateDate}).ToList();
            ViewBag.ContractList = result;
            return View();
        }
        public ActionResult HistoryContract()
        {
            var id = Session["UserID"];
            var idUser = Convert.ToInt32(id);
            var result = (from c in db.HistoryContracts join o in db.OrderDetails on c.ContractID equals o.ContractID
                          join i in db.Insurances on o.InsurantID equals i.ID
                          where c.UserID == idUser
                          select new {Name = c.ContractName, startDate = c.StartDate, endDate = c.EndDate
                          , address = c.DeliveryAddress, InsuranceID = i.ID
                          , contractID = c.ContractID, status = c.Status
                          , updateDate = c.UpdateDate}).ToList();
            ViewBag.ContractList = result;
            return View();
        }
        public JsonResult GetODandPayment(string id1)
        {
            DataModel db = new DataModel();
            var id = Convert.ToInt32(Session["UserID"].ToString());
            var ContractID = Convert.ToInt32(id1);
            var payment = new PayMent();
            var itemNumber = Convert.ToInt32(payment.ItemNumber);

            var History = (from c in db.Contracts
                           join o in db.OrderDetails on c.ID equals o.ContractID
                           where c.UserID == id && c.ID == ContractID
                           select o).SingleOrDefault();

            var Pay = db.PayMents.Where(x=>x.ItemNumber == id1);

            return Json(new
            {
                value = History,
                value1 = Pay,
                JsonRequestBehavior.AllowGet
            });

        }

        public ActionResult Charge()
        {
            var result = db.Insurances.Where(x => x.Status == 1).ToList();
            return View(result);
        }
    }


}
