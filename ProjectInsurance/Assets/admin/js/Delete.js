﻿var cart = {
    init: function () {
        cart.regEvents();
    },
    regEvents: function () {
        $('.delete_PolicyInsurance').off('click').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var insuranceID = $('#InsuranceID').val();
            var con = confirm("Bạn có muốn xóa chính sách này không");
            if (con) {
                $.ajax({
                    url: "/AdminManager/PolicyInsuranceAdmin/Delete/" + id,
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        if (res.status == true) {
                            swal("Thông báo", "Xóa chính sách thành công.", "sucess")
                                .then((value) => {
                                    window.location.href = "/AdminManager/PolicyInsuranceAdmin/Index/" + insuranceID;
                                })
                        } else {
                            swal({
                                title: "Thông báo",
                                text: "Chưa xóa được bảo hiểm.",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true
                            }).then((value) => {
                                window.location.href = "/AdminManager/PolicyInsuranceAdmin/Index/" + InsuranceID;
                            })
                        }
                    }
                })
            }
        });
        $('.delete_Insurance').off('click').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var cateID = $("#cateID").val();
            var con = confirm("Bạn có muốn xóa bảo hiểm này không");
            if (con) {
                $.ajax({
                    url: "/AdminManager/InsuranceAdmin/Delete/" + id,
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        if (res.status == true) {
                            swal("Thông báo", "Xóa bảo hiểm thành công.", "success")
                                .then((value) => {
                                    window.location.href = "/chi-tiet/" + cateID;
                                });
                        } else {
                            swal({
                                title: "Thông báo",
                                text: "Chưa xóa được bảo hiểm.",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true
                            })
                                .then((value) => {
                                    window.location.href = "/chi-tiet/" + cateID;
                                });
                        }
                    }
                });
            }
        });
        $('.delete_InsuranceCategory').off('click').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            $.ajax({
                url: "/AdminManager/InsuranceCategoryAdmin/Delete/" + id,
                type: "POST",
                dataType: "json",
                success: function (res) {
                    if (res.status == true) {
                        window.location.href = "/AdminManager/InsuranceCategoryAdmin/Index";
                    }
                }
            })
        });
        $('.delete_New').off('click').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var cateNewID = $("#cateNewID").val();
            var con = confirm("Bạn có muốn xóa tin này không");
            if (con) {
                $.ajax({
                    url: "/AdminManager/NewAdmin/Delete/" + id,
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        if (res.status == true) {
                            swal("Thông báo", "Xóa tin thành công.", "success")
                                .then((value) => {
                                    window.location.href = "/de-an-chien-luoc/" + cateNewID;
                                });
                        } else {
                            swal({
                                title: "Thông báo",
                                text: "Chưa xóa được tin.",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true
                            })
                                .then((value) => {
                                    window.location.href = "/de-an-chien-luoc/" + cateNewID;
                                });
                        }
                    }
                });
            }
        });
        $('.delete_Contract').off('click').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            //var cateID = $("#cateID").val();
            var con = confirm("Bạn có muốn khóa hợp đồng này không");
            if (con) {
                $.ajax({
                    url: "/AdminManager/ContractAdmin/Delete/" + id,
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        if (res.status == true) {
                            swal("Thông báo", "Khóa hợp đồng thành công.", "success")
                                .then((value) => {
                                    window.location.href = "/hop-dong";
                                });
                        } else {
                            swal({
                                title: "Thông báo",
                                text: "Chưa khóa được hợp đồng.",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true
                            })
                                .then((value) => {
                                    window.location.href = "/hop-dong";
                                });
                        }
                    }
                });
            }
        });
    }
}
cart.init();
