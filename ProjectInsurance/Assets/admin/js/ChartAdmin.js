﻿$(document).ready(function () {
    var start = moment().subtract(29, 'days');
    var end = moment();
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        jsonThongKeTheoLop(function (data) {
            $.each(data, function (i, item) {
                Name.push([item.Name].toString());
                Amount.push([item.Amount]);
                datas = data;
            });
            google.setOnLoadCallback(drawChartSv);
            google.setOnLoadCallback(drawChart);
        });
        jsonThongKeDangHoatDong(function (data) {
            $.each(data, function (i, item) {
                Name1.push([item.Name].toString());
                Amount1.push([item.Amount]);
                datas1 = data;
            });
            google.setOnLoadCallback(drawChartSv1);
            google.setOnLoadCallback(drawChart1);
        });

        GetRevenue();
    };

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    cb(start, end);
    $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
        jsonThongKeTheoLop(function (data) {
            $.each(data, function (i, item) {
                Name.push([item.Name].toString());
                Amount.push([item.Amount]);
                datas = data;
            });
            google.setOnLoadCallback(drawChartSv);
            google.setOnLoadCallback(drawChart);
        });
        jsonThongKeDangHoatDong(function (data) {
            $.each(data, function (i, item) {
                Name1.push([item.Name].toString());
                Amount1.push([item.Amount]);
                datas1 = data;
            });
            google.setOnLoadCallback(drawChartSv1);
            google.setOnLoadCallback(drawChart1);
        });

        GetRevenue();
    });

    function jsonThongKeTheoLop(handelData) {
        var date = $("#reportrange").val();
        var startDate = date.slice(0, 10);
        var endDate = date.slice(13, 23);
        $.ajax({
            url: "/AdminManager/Chart/GetJsonData",
            type: "GET",
            data: {
                startDate: startDate,
                endDate: endDate
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (result) {
                handelData(result);
                drawChartSv(result);
                drawChart(result);
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
        return false;
    };

    function jsonThongKeDangHoatDong(handleData) {
        var date = $("#reportrange").val();
        var startDate = date.slice(0, 10);
        var endDate = date.slice(13, 23);
        $.ajax({
            url: "/AdminManager/Chart/GetJsonData1",
            type: "GET",
            data: {
                startDate: startDate,
                endDate: endDate
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (result) {
                handleData(result);
                drawChart1(result);
                drawChartSv1(result);
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
        return false;
    };

    function GetRevenue() {
        var date = $("#reportrange").val();
        var startDate = date.slice(0, 10);
        var endDate = date.slice(13, 23);
        $.ajax({
            url: '/AdminManager/Chart/Revenue?startDate=' +
                startDate +
                '&endDate=' +
                endDate,
            method: 'GET',
            success: function (resp) {
                var json = $.parseJSON(resp);
                if (json.length == 0) {
                    swal('Notifycation', 'Revenue Nodata', 'warning');
                    return;
                }
                drawChartRevenue(json);
            },
            error: function () {
                swal('error');
            }
        });
    }

});
google.load("visualization", "1", { packages: ["corechart"] });

var Name = [];
var Amount = [];
var datas;

function drawChartSv(result) {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Name');
    data.addColumn('number', 'Amount');

    data.addRows(Name.length);

    for (var i = 0; i < result.length; i++) {
        data.setCell(i, 0, result[i].Name + ' (' + result[i].Amount + ' contract)');
        data.setCell(i, 1, parseInt(result[i].Amount));
    }
    var options = {
        'title': 'Statistics of the contract number of insurance packages.', titleTextStyle: { color: 'blue' },
        is3D: true
    };
    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    chart.draw(data, options);
};
function drawChart() {
    var dataArray = [['Insurance Name', 'Amount', 'Rank']];
    var xephang = 0;
    $.each(datas, function (i, item) {
        xephang += 1;
        if (item.Amount != 0) {
            dataArray.push([item.Name, item.Amount, xephang]);
        }
    });

    var data = new google.visualization.arrayToDataTable(dataArray);
    var options = {
        title: 'Compare the number of contracts of insurance packages.', titleTextStyle: { color: 'blue' },
        hAxis: { title: 'Tên bảo hiểm', titleTextStyle: { color: 'blue' } }
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));

    chart.draw(data, options);

};

google.load("visualization", "1", { packages: ["corechart"] });

var Name1 = [];
var Amount1 = [];
var datas1;

function drawChartSv1(result) {
    var data1 = new google.visualization.DataTable();
    data1.addColumn('string', 'Name');
    data1.addColumn('number', 'Amount');

    data1.addRows(Name.length);

    for (var i = 0; i < result.length; i++) {
        data1.setCell(i, 0, result[i].Name + ' (' + result[i].Amount + ' contract)');
        data1.setCell(i, 1, parseInt(result[i].Amount));
    }
    var options1 = {
        'title': 'Statistics of the number of operating contracts of insurance packages.', titleTextStyle: { color: 'blue' },
        is3D: true
    };
    var chart1 = new google.visualization.PieChart(document.getElementById('piechart1'));
    chart1.draw(data1, options1);
}
function drawChart1() {
    var dataArray1 = [['Insurance Name', 'Amount', 'Rank']];
    var xephang1 = 0;
    $.each(datas1, function (i, item) {
        xephang1 += 1;
        if (item.Amount != 0) {
            dataArray1.push([item.Name, item.Amount, xephang1]);
        }
    });

    var data1 = new google.visualization.arrayToDataTable(dataArray1);
    var options1 = {
        title: 'Compare the number of active contracts of insurance packages.', titleTextStyle: { color: 'blue' },
        hAxis: { title: 'Tên bảo hiểm', titleTextStyle: { color: 'blue' } }
    };

    var chart1 = new google.visualization.ColumnChart(document.getElementById('chart_div1'));

    chart1.draw(data1, options1);

}

google.load("visualization", "1", { packages: ["line"] });

function drawChartRevenue(result) {
    var data3 = new google.visualization.DataTable();
    data3.addColumn('date', 'Date');
    data3.addColumn('number', 'Revenue');
    for (var i = 0; i < result.length; i++) {
        data3.addRow([new Date(result[i].Date), Number(result[i].Total)]);
    }
    var options3 = {
        chart: {
            title: 'Revenue chart over time',
            subtitle: '(VNĐ)'
        },
        height: 500,
        hAxis: {
            format: 'MM/dd/yyyy'
        }
    };
    var chart3 = new google.charts.Line(document.getElementById('RevenueChart'));
    chart3.draw(data3, google.charts.Line.convertOptions(options3));
};
