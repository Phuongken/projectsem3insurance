﻿
function Finter(id, startdate, enddate, search, idStatus, idDuration) {
    $.ajax({
        url: "/EmployeeManager/ContractAdmin/Finter/",
        type: "POST",
        data: {
            id: id,
            startDate: startdate,
            endDate: enddate,
            search: search,
            idStatus: idStatus,
            idDuration: idDuration
        },
        dataType: "json",
        success: function (res) {
            if (res.value[0] != null) {
                var newHtml = "";
                $.each(res.value, function (index, value) {

                    function parseDate1(date) {
                        var str = date;
                        var res1 = str.toString().replace("/Date(", '');
                        var res2 = res1.toString().replace(")/", '');
                        var newDate = new Date(parseInt(res2));
                        newDate.setDate(newDate.getDate());
                        return newDate;
                    };
                    function datetimeNew(date) {
                        var dd = parseDate1(date).getDate();
                        var mm = parseDate1(date).getMonth() + 1;
                        var y = parseDate1(date).getFullYear();
                        return end = dd + '-' + mm + '-' + y;
                    };

                    function getYear(date) {
                        return parseDate1(date).getFullYear();
                    };



                    function getMonth(date) {
                        return parseInt(parseDate1(date).getMonth() + 1);
                    };

                    function checktatus(status, endDate) {
                        var statusHtml = "";
                        if (status == 2 && parseDate1(endDate) > new Date()) {
                            statusHtml = "<p data-id=" + '"' + "dangXuLy" + '"' + "class=" + '"' + "btn-dangXuLy" + '"' + "style=" + '"' + "background-color:green;color:white; text-align:center;border-radius:5px;" + '"' + ">Pending</p>";
                        }
                        else if (status == 1 && parseDate1(endDate) > new Date()) {
                            statusHtml = "<p data-id=" + '"' + "hoatDong" + '"' + "class=" + '"' + "btn-hoatDong" + '"' + "style=" + '"' + "background-color:yellowgreen;color:white; text-align:center;border-radius:5px;" + '"' + ">Open</p>";
                        }
                        else if (status == 0) {
                            statusHtml = "<p style=" + '"' + "background-color:red;color:white; text-align:center;border-radius:5px;" + '"' + ">Lock</p>";
                        } else {
                            statusHtml = "<p style=" + '"' + "background-color:red;color:white; text-align:center;border-radius:5px;" + '"' + ">Lock</p>";
                        }
                        return statusHtml;
                    };

                    function checkAcction(status, endDate) {
                        var acttionHtml = "";
                        var date = new Date();
                        var year = date.getFullYear();
                        if (getYear(endDate) == year) {
                            if (status == 0) {
                                acttionHtml = "<a  href=" + '"' + "/EmployeeManager/ContractAdmin/SendMailOpenContract/" +
                                    value.Contract.ID + '"' + "data-id=" + '"' + value.Contract.ID + '"' + "class=" + '"'
                                    + "btn-open" + '"' + ">Open</a>"
                                    + "<label>|</label>"
                                    + "<a href=" + '"' + "/EmployeeManager/ContractAdmin/Details/" + value.Contract.ID + '"' + ">Detail</a>";
                            }
                            else if (parseDate1(endDate) < new Date()) {
                                acttionHtml = "<a href=" + '"' + "/AdminManager/ContractAdmin/reOpen/" +
                                    value.Contract.ID + '"' + "class=" + '"' + "btn-molai" + '"' + ">Re-open</a>"
                                    + "<label>|</label>"
                                    + "<a href=" + '"' + "/EmployeeManager/ContractAdmin/Details/" + value.Contract.ID + '"' + ">Detail</a>";
                            }
                            else if ((parseInt(getMonth(endDate)) - date.getMonth()) == 1) {
                                acttionHtml = "<p style=" + '"' + "color:red;" + '"' + ">Expire coming</p>"
                                    + "<a href=" + '"' + "#" + '"' + "data-id=" + '"' + value.Contract.ID + '"' + "class=" + '"' + "btn-giahan" + '"' + ">Renew</a>"
                                    + "<label>|</label>"
                                    + "<a href=" + '"' + "#" + '"' + "data-id=" + '"' + value.Contract.ID + '"' + "class=" + '"'
                                    + "delete_Contract" + '"' + ">Lock</a>"
                                    + "<label>|</label>"
                                    + "<a href=" + '"' + "/EmployeeManager/ContractAdmin/Details/" + value.Contract.ID + '"' + ">Detail</a>";
                            }
                            else if ((parseInt(getMonth(endDate)) - date.getMonth()) == 1 || (parseInt(getMonth(endDate)) - date.getMonth()) == 0) {
                                acttionHtml = "<p style=" + '"' + "color:red;" + '"' + ">Expire coming</p>"
                                    + "<a href=" + '"' + "#" + '"' + "data-id=" + '"' + value.Contract.ID + '"' + "class=" + '"' + "btn-giahan" + '"' + ">Renew</a>"
                                    + "<label>|</label>"
                                    + "<a href=" + '"' + "#" + '"' + "data-id=" + '"' + value.Contract.ID + '"'
                                    + "class=" + '"' + "delete_Contract" + '"' + ">Lock</a>"
                                    + "<label>|</label>"
                                    + "<a href=" + '"' + "/EmployeeManager/ContractAdmin/Details/" + value.Contract.ID + '"' + ">Detail</a>";
                            }
                            else {

                                if (status == 2 && parseDate1(endDate) > new Date()) {
                                    acttionHtml = "<a style=" + '"' + "cursor:pointer;" + '"' + "data-id=" + '"' + value.Contract.ID + '"' + "class=" + '"' + "btn-active" + '"' + ">Active</a>"
                                        + "<a href=" + '"' + "/EmployeeManager/ContractAdmin/Edit/" + value.Contract.ID + '"' + ">Edit</a>"
                                        + "<label>|</label>"
                                        + "<a href=" + '"' + "/EmployeeManager/ContractAdmin/Details/" + value.Contract.ID + '"' + ">Detail</a>";
                                }
                                else {
                                    acttionHtml = "<a href=" + '"' + "#" + '"' + "data-id=" + '"' +
                                        value.Contract.ID + '"' + "class=" + '"' + "delete_Contract" + '"' + ">Lock</a>"
                                        + "<label>|</label>"
                                        + "<a href=" + '"' + "/EmployeeManager/ContractAdmin/Details/" + value.Contract.ID + '"' + ">Detail</a>";

                                }

                            }
                        } else {
                            if (status == 2 && parseDate1(endDate) > new Date()) {
                                acttionHtml = "<a style=" + '"' + "cursor:pointer;" + '"' + "data-id=" + '"' + value.Contract.ID + '"' + "class=" + '"' + "btn-active" + '"' + ">Active</a>"
                                    + "<a href=" + '"' + "/EmployeeManager/ContractAdmin/Edit/" + value.Contract.ID + '"' + ">Edit</a>"
                                    + "<label>|</label>"
                                    + "<a href=" + '"' + "#" + '"' + "data-id=" + '"' + value.Contract.ID + '"' + "class=" + '"' + "delete_Contract" + '"' + ">Cancel</a>"
                                    + "<label>|</label>"
                                    + "<a href=" + '"' + "/EmployeeManager/ContractAdmin/Details/" + value.Contract.ID + '"' + ">Detail</a>";
                            }
                            else if (status == 0) {
                                acttionHtml = "<a href=" + '"' + "/EmployeeManager/ContractAdmin/SendMailOpenContract/"
                                    + value.Contract.ID + '"' + "data-id=" + '"' + value.Contract.ID + '"' +
                                    "class=" + '"' + "btn-open" + '"' + ">Open</a>"
                                    + "<label>|</label>"
                                    + "<a href=" + '"' + "/EmployeeManager/ContractAdmin/Details/" + value.Contract.ID + '"' + ">Detail</a>";
                            }
                            else {
                                acttionHtml = "<a href=" + '"' + "#" + '"' + "data-id=" + '"' + value.Contract.ID + '"' +
                                    "class=" + '"' + "delete_Contract" + '"' + ">Lock</a>"
                                    + "<label>|</label>"
                                    + "<a href=" + '"' + "/EmployeeManager/ContractAdmin/Details/" + value.Contract.ID + '"' + ">Detail</a>";

                            }
                        }
                        return acttionHtml;
                    };

                    newHtml +=
                        '<tr>'
                        + "<td>" + value.Contract.ID + "</td>"
                        + "<td>" + value.Contract.UserID + "</td>"
                        + "<td>"
                        + "<p>" + value.OrderDetail.FullName + "</p>"
                        + "<p>" + value.OrderDetail.Email + "</p>"
                        + "<p>" + value.OrderDetail.MobilePhone + "</p>"
                        + "</td>"
                        + "<td>" + value.Contract.ContractName + "</td>"
                        + "<td>" + datetimeNew(value.Contract.StartDate) + "</td>"
                        + "<td>" + datetimeNew(value.Contract.EndDate) + "</td>"
                        + "<td>" + value.Contract.PaymentAddress + "</td>"
                        + "<td>" + value.Contract.DeliveryAddress + "</td>"
                        + "<td>" + checktatus(value.Contract.Status, value.Contract.EndDate) + "</td>"
                        + "<td>" + checkAcction(value.Contract.Status, value.Contract.EndDate) + "</td>"
                    '</tr>'
                });
                $('.changeContract').html(newHtml);
                $(document).ready(function () {
                    $('#datatable').dataTable();
                });


                $('.btn-active').off('click').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var con = confirm("Bạn có muốn xác nhận hợp đồng không");
                    if (con) {
                        $.ajax({
                            url: "/EmployeeManager/ContractAdmin/ChangeStatus/" + id,
                            type: "POST",
                            dataType: "json",
                            success: function (res) {
                                if (res.status == true) {
                                    swal("Thông báo", "Xác nhận hợp đồng thành công.", "success")
                                        .then((value) => {
                                            window.location.href = "/hop-dong-employee";
                                        });
                                } else {
                                    swal({
                                        title: "Thông báo",
                                        text: "Chưa xác nhận được hợp đồng.",
                                        icon: "warning",
                                        buttons: true,
                                        dangerMode: true
                                    })
                                        .then((value) => {
                                            window.location.href = "/hop-dong-employee";
                                        });
                                }
                            }
                        });
                    }
                });
                $('.btn-open').off('click').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var con = confirm("Bạn có muốn mở hợp đồng không");
                    if (con) {
                        $.ajax({
                            url: "/EmployeeManager/ContractAdmin/ChangeStatusOpen/" + id,
                            type: "POST",
                            dataType: "json",
                            success: function (res) {
                                if (res.status == true) {
                                    swal("Thông báo", "Mở hợp đồng thành công.", "success")
                                        .then((value) => {
                                            window.location.href = "/hop-dong-employee";
                                        });
                                } else {
                                    swal({
                                        title: "Thông báo",
                                        text: "Chưa mở được hợp đồng.",
                                        icon: "warning",
                                        buttons: true,
                                        dangerMode: true
                                    })
                                        .then((value) => {
                                            window.location.href = "/hop-dong-employee";
                                        });
                                }
                            }
                        });
                    }
                });
                // gia han
                $('.btn-giahan').off('click').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var con = confirm("Bạn có muốn gia hạn hợp đồng không");
                    if (con) {
                        $.ajax({
                            url: "/EmployeeManager/ContractAdmin/SendMailInfo/" + id,
                            type: "POST",
                            dataType: "json",
                            success: function (res) {
                                if (res.status == true) {
                                    swal("Thông báo", "Gia hạn hợp đồng thành công.", "success")
                                        .then((value) => {
                                            window.location.href = "/hop-dong-employee";
                                        });
                                } else {
                                    swal({
                                        title: "Thông báo",
                                        text: "Chưa gia hạn được hợp đồng.",
                                        icon: "warning",
                                        buttons: true,
                                        dangerMode: true
                                    })
                                        .then((value) => {
                                            window.location.href = "/hop-dong-employee";
                                        });
                                }
                            }
                        });
                    }
                });

                $('.delete_Contract').off('click').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var con = confirm("Bạn có muốn khóa hợp đồng này không");
                    if (con) {
                        $.ajax({
                            url: "/EmployeeManager/ContractAdmin/Delete/" + id,
                            type: "POST",
                            dataType: "json",
                            success: function (res) {
                                if (res.status == true) {
                                    swal("Thông báo", "Khóa hợp đồng thành công.", "success")
                                        .then((value) => {
                                            window.location.href = "/hop-dong-employee";
                                        });
                                } else {
                                    swal({
                                        title: "Thông báo",
                                        text: "Chưa khóa được hợp đồng.",
                                        icon: "warning",
                                        buttons: true,
                                        dangerMode: true
                                    })
                                        .then((value) => {
                                            window.location.href = "/hop-dong-employee";
                                        });
                                }
                            }
                        });
                    }
                });

            } else {
                swal({
                    title: "Notification",
                    text: "No data you requested.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true
                }).then((value) => {
                    window.location.href = "/hop-dong-employee";
                });
            }
        }
    });
};
$(function () {
    $('input[name="daterange"]').daterangepicker({
        opens: 'left'
    }, function (start, end, label) {
        var search = $("#InputDate").val();
        var id = $("#InsuranceIDFinder").val();
        var idStatus = $("#InsuranceStatusFinder").val();
        var idDuration = $("#InsuranceDurationFinder").val();
        Finter(id, start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'), search, idStatus, idDuration);
    });
});
$(".Finter").off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data("id");
    var search = $("#InputDate").val();
    var date = $("#DateFinter").val();
    var startDate = date.slice(0, 10);
    var endDate = date.slice(13, 23);
    var idStatus = $("#InsuranceStatusFinder").val();
    var idDuration = $("#InsuranceDurationFinder").val();
    Finter(id, startDate, endDate, search, idStatus, idDuration);
    $("#InsuranceIDFinder").val(id);
    $(".Finter").css("background", "#1b93e1");
    if ($("#InsuranceIDFinder").val() == id) {
        $(this).css("background", "#e74c3c");
    }
});
$("#SearchFinder").off('click').on('click', function (e) {
    e.preventDefault();
    var search = $("#InputDate").val();
    var date = $("#DateFinter").val();
    var startDate = date.slice(0, 10);
    var endDate = date.slice(13, 23);
    var id = $("#InsuranceIDFinder").val();
    var idStatus = $("#InsuranceStatusFinder").val();
    var idDuration = $("#InsuranceDurationFinder").val();
    Finter(id, startDate, endDate, search, idStatus, idDuration);
});

$(".StatusInsurance").off('click').on('click', function (e) {
    e.preventDefault();
    var id = $("#InsuranceIDFinder").val();
    var idStatus = $(this).data("id");
    var idDuration = $("#InsuranceDurationFinder").val();
    var search = $("#InputDate").val();
    var date = $("#DateFinter").val();
    var startDate = date.slice(0, 10);
    var endDate = date.slice(13, 23);
    Finter(id, startDate, endDate, search, idStatus, idDuration);
    $("#InsuranceStatusFinder").val(idStatus);
    $(".StatusInsurance").css("background", "#ff9501");
    if ($("#InsuranceStatusFinder").val() == idStatus) {
        $(this).css("background", "#e74c3c");
    }
});

$('.ExpireInsurance').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $("#InsuranceIDFinder").val();
    var idStatus = $("#InsuranceStatusFinder").val();
    var idDuration = $(this).data("id");
    var search = $("#InputDate").val();
    var date = $("#DateFinter").val();
    var startDate = date.slice(0, 10);
    var endDate = date.slice(13, 23);
    Finter(id, startDate, endDate, search, idStatus, idDuration);
    $("#InsuranceDurationFinder").val(idDuration);
    $(".ExpireInsurance").css("background", "#a2d200");
    if ($("#InsuranceDurationFinder").val() == idDuration) {
        $(this).css("background", "#e74c3c");
    }
});
$(document).ready(function () {
    $('#datatable').dataTable();
    $('.wysihtml5').wysihtml5();
});
$('.btn-active').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Do you want to confirm this contract?");
    if (con) {
        $.ajax({
            url: "/EmployeeManager/ContractAdmin/ChangeStatus/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    swal("Thông báo", "Confirm success ", "success")
                        .then((value) => {
                            window.location.href = "/hop-dong-employee";
                        });
                } else {
                    swal({
                        title: "Thông báo",
                        text: "Cannot confirm this contract",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    })
                        .then((value) => {
                            window.location.href = "/hop-dong-employee";
                        });
                }
            }
        });
    }
});
// open contract
$('.btn-open').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Do you want to open this contract");
    if (con) {
        $.ajax({
            url: "/EmployeeManager/ContractAdmin/ChangeStatusOpen/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    swal("Thông báo", "Contract open  success", "success")
                        .then((value) => {
                            window.location.href = "/hop-dong-employee";
                        });
                } else {
                    swal({
                        title: "Thông báo",
                        text: "Cannot open this contract",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    })
                        .then((value) => {
                            window.location.href = "/hop-dong-employee";
                        });
                }
            }
        });
    }
});
// gia han
$('.btn-giahan').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Do you want to renew this contract?");
    if (con) {
        $.ajax({
            url: "/EmployeeManager/ContractAdmin/SendMailInfo/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    swal("Thông báo", "Renew contract success", "success")
                        .then((value) => {
                            window.location.href = "/hop-dong-employee";
                        });
                } else {
                    swal({
                        title: "Thông báo",
                        text: "Cannot renew this contract",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    })
                        .then((value) => {
                            window.location.href = "/hop-dong-employee";
                        });
                }
            }
        });
    }
});