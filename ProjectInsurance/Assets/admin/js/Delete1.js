﻿var cart = {
    init: function () {
        cart.regEvents();
    },
    regEvents: function () {
        $('.delete_PolicyInsurance').off('click').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var insuranceID = $('#InsuranceID').val();
            var con = confirm("Bạn có muốn xóa chính sách này không");
            if (con) {
                $.ajax({
                    url: "/EmployeeManager/PolicyInsuranceAdmin/Delete/" + id,
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        if (res.status == true) {
                            swal("Thông báo", "Xóa chính sách thành công.", "sucess")
                                .then((value) => {
                                    window.location.href = "/EmployeeManager/PolicyInsuranceAdmin/Index/" + insuranceID;
                                })
                        } else {
                            swal({
                                title: "Thông báo",
                                text: "Chưa xóa được bảo hiểm.",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true
                            }).then((value) => {
                                window.location.href = "/EmpoyeeManager/PolicyInsuranceAdmin/Index/" + InsuranceID;
                            })
                        }
                    }
                })
            }
        });
        $('.delete_Insurance').off('click').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var cateID = $("#cateID").val();
            var con = confirm("Bạn có muốn xóa bảo hiểm này không");
            if (con) {
                $.ajax({
                    url: "/EmployeeManager/InsuranceAdmin/Delete/" + id,
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        if (res.status == true) {
                            swal("Thông báo", "Xóa bảo hiểm thành công.", "success")
                                .then((value) => {
                                    window.location.href = "/chi-tiet-employee/" + cateID;
                                });
                        } else {
                            swal({
                                title: "Thông báo",
                                text: "Chưa xóa được bảo hiểm.",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true
                            })
                                .then((value) => {
                                    window.location.href = "/chi-tiet-employee/" + cateID;
                                });
                        }
                    }
                });
            }
        });
        $('.delete_InsuranceCategory').off('click').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            $.ajax({
                url: "/EmployeeManager/InsuranceCategoryAdmin/Delete/" + id,
                type: "POST",
                dataType: "json",
                success: function (res) {
                    if (res.status == true) {
                        window.location.href = "/EmpoyeeManager/InsuranceCategoryAdmin/Index";
                    }
                }
            })
        });
        $('.delete_New').off('click').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var cateNewID = $("#cateNewID").val();
            var con = confirm("Bạn có muốn xóa tin này không");
            if (con) {
                $.ajax({
                    url: "/EmployeeManager/NewAdmin/Delete/" + id,
                    type: "POST",
                    dataType: "json",
                    success: function (res) {
                        if (res.status == true) {
                            swal("Thông báo", "Xóa tin thành công.", "success")
                                .then((value) => {
                                    window.location.href = "/de-an-chien-luoc-employee/" + cateNewID;
                                });
                        } else {
                            swal({
                                title: "Thông báo",
                                text: "Chưa xóa được tin.",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true
                            })
                                .then((value) => {
                                    window.location.href = "/de-an-chien-luoc-employee/" + cateNewID;
                                });
                        }
                    }
                });
            }
        });
    }
}
cart.init();
