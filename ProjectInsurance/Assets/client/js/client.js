﻿
var heightmax = Math.max.apply(null, $(".chinhsach").map(function () {
    return $(this).height();
}).get());
$(window).load(function () {
    $(".chinhsach").height(heightmax);
});
$(".xemdong").click(function () {
    if ($(".xemdong").attr('value') === "Đóng lại") {
        $(".xemdong").attr('value', "Xem thêm");
        $(".chinhsach").height(50);
    } else {
        $(".xemdong").attr('value', "Đóng lại");
        $(".chinhsach").height(heightmax);
    }
    $(".chinh").toggle("slow");
    $(".chinh1").hide("slow");
    $(".chinh2").hide("slow");
    $(".uudiem").height(50);
    $(".infochitiet").height(50);
    $(".xemdong1").attr('value', "Xem thêm");
    $(".xemdong2").attr('value', "Xem thêm");
});
var heightmax1 = Math.max.apply(null, $(".uudiem").map(function () {
    return $(this).height();
}).get());
$(window).load(function () {
    $(".chinh1").hide();
});
$(".xemdong1").click(function () {
    if ($(".xemdong1").attr('value') === "Xem thêm") {
        $(".xemdong1").attr('value', "Đóng lại");
        $(".uudiem").height(heightmax1);
    } else {
        $(".xemdong1").attr('value', "Xem thêm");
        $(".uudiem").height(50);
    }
    $(".chinh").hide("slow");
    $(".chinh1").toggle("slow");
    $(".chinh2").hide("slow");
    $(".chinhsach").height(50);
    $(".infochitiet").height(50);
    $(".xemdong").attr('value', "Xem thêm");
    $(".xemdong2").attr('value', "Xem thêm");
});
var heightmax2 = Math.max.apply(null, $(".infochitiet").map(function () {
    return $(this).height();
}).get());
$(window).load(function () {
    $(".chinh2").hide();
});
$(".xemdong2").click(function () {
    if ($(".xemdong2").attr('value') === "Xem thêm") {
        $(".xemdong2").attr('value', "Đóng lại");
        $(".infochitiet").height(heightmax2);
    } else {
        $(".xemdong2").attr('value', "Xem thêm");
        $(".infochitiet").height(50);
    }
    $(".chinh").hide("slow");
    $(".chinh1").hide("slow");
    $(".chinh2").toggle("slow");
    $(".uudiem").height(50);
    $(".chinhsach").height(50);
    $(".xemdong1").attr('value', "Xem thêm");
    $(".xemdong").attr('value', "Xem thêm");
});

$(window).load(function () {
    $(".hideshowchinhsach").hide();
    if ($("#startDate").val() != "") {
        $(".ngayketthuc").show();
    } else {
        $(".ngayketthuc").hide();
    }

    if ($("#PriceAffterFee").val() != "") {
        $(".feeHouse").show();
    } else {
        $(".feeHouse").hide();
    }
});
$(".updown").click(function () {
    $(".hideshowchinhsach").toggle("slow");
    $(".changeclass").toggleClass("fa-angle-up");
});

$("#EnterPrice").change(function () {
    if ($("#EnterPrice").val() != "") {
        var priceHouse = parseInt($("#EnterPrice").val());
        var fee = parseFloat($("#FeeInsurance").val());
        var result = 0;
        if (300000000 < priceHouse < 2000000000) {
            result = ((fee / 100) * priceHouse) - ((10 / 100) * ((fee / 100) * priceHouse));
        } else if (2000000000 < priceHouse) {
            fee = fee - 0.09;
            result = ((fee / 100) * priceHouse) - ((10 / 100) * ((fee / 100) * priceHouse));
        }
        $("#PriceAffterFee").attr('value', result);
        $(".feeHouse").show();
    } else {
        $(".feeHouse").hide();
    }
});

$("#startDate").change(function () {
    if ($("#startDate").val() != "") {
        var term = parseInt($("#term  option:selected").val());
        var date = new Date($("#startDate").val());
        var newDate = new Date(date);
        newDate.setDate(newDate.getDate());
        var dd = newDate.getDate();
        var mm = newDate.getMonth() + 1;
        var y = newDate.getFullYear() + term;
        var end = mm + '-' + dd + '-' + y;
        $("#endDate").attr('value', end);
        $(".ngayketthuc").show();
    } else {
        $(".ngayketthuc").hide();
    }
});

$("#term").change(function () {
    var term = parseInt($("#term  option:selected").val());
    var date = new Date($("#endDate").val());
    var newDate = new Date(date);
    newDate.setDate(newDate.getDate());
    var dd = newDate.getDate();
    var mm = newDate.getMonth() + 1;
    var y = newDate.getFullYear() + term;
    var end = mm + '-' + dd + '-' + y;
    $("#endDate").attr('value', end);
});


$(document).ready(function () {

    $('#Provind').on('change', function () {
        var txtProvind = $('#Provind option:selected').val();
        $.ajax({
            type: 'GET',
            data: { txtDistrict: txtProvind },
            url: '/Order/GetAllDistrictByProvind',
            success: function (result) {
                var s = '<option value="0">Quận</option>';
                var p = '<option value="0">Phường</option>';
                for (var i = 0; i < result.length; i++) {
                    s += '<option value="' + result[i].ID + '">' + result[i].Name + '</option>';
                }
                $('#District').html(s);
                $('#Ward').html(p);
            }
        });
    });
    $('#District').on('change', function () {
        var txtDistrict = $('#District').val();
        $.ajax({
            type: 'GET',
            data: { txtWard: txtDistrict },
            url: '/Order/GetAllWardByDistrict',
            success: function (result) {
                var s = '<option value="0">Phường</option>';
                for (var i = 0; i < result.length; i++) {
                    s += '<option value="' + result[i].ID + '">' + result[i].Name + '</option>';
                }
                $('#Ward').html(s);
            }
        });
    });


    $('#Provind1').on('change', function () {
        var txtProvind = $('#Provind1 option:selected').val();
        $.ajax({
            type: 'GET',
            data: { txtDistrict: txtProvind },
            url: '/Order/GetAllDistrictByProvind',
            success: function (result) {
                var s = '<option value="0">Quận</option>';
                var p = '<option value="0">Phường</option>';
                for (var i = 0; i < result.length; i++) {
                    s += '<option value="' + result[i].ID + '">' + result[i].Name + '</option>';
                }
                $('#District1').html(s);
                $('#Ward1').html(p);
            }
        });
    });
    //
    $('#District1').on('change', function () {
        var txtDistrict = $('#District1').val();
        $.ajax({
            type: 'GET',
            data: { txtWard: txtDistrict },
            url: '/Order/GetAllWardByDistrict',
            success: function (result) {
                var s = '<option value="0">Phường</option>';
                for (var i = 0; i < result.length; i++) {
                    s += '<option value="' + result[i].ID + '">' + result[i].Name + '</option>';
                }
                $('#Ward1').html(s);
            }
        });
    });

    $("#addressCheckbox").off('click').on('click', function (e) {
        var data = $(this).attr('data-check');

        if (data == "avaiable") {
            $(this).attr('data-check', "");
            var addressDetail = $("#AddressDetail").val();
            var provind = $("#Provind").val();
            var district = $("#District").val();
            var ward = $("#Ward").val();

            $("#AddressDetail1").val(addressDetail);
            $("#AddressDetail1").attr("readonly", "readonly");
            $("#AddressDetail1").css("background", "#f3f4f6");
            $.ajax({
                type: 'GET',
                data: {
                    provindID: provind,
                    districtID: district,
                    wardID: ward
                },
                url: '/Order/GetAddress',
                success: function (result) {

                    if (result.provind != null && result.district != null && result.ward != null) {
                        var provind = '<option value="' + result.provind.ID + '">' + result.provind.Name + '</option>';
                        var district = '<option value="' + result.district.ID + '">' + result.district.Name + '</option>';
                        var ward = '<option value="' + result.ward.ID + '">' + result.ward.Name + '</option>';
                    } else if (result.provind == null) {
                        var provind = '<option value="0">Thành phố</option>';
                        var district = '<option value="0">Quận</option>';
                        var ward = '<option value="0">Phường</option>';
                    }
                    else if (result.district == null && result.ward == null) {
                        var provind = '<option value="' + result.provind.ID + '">' + result.provind.Name + '</option>';
                        var district = '<option value="0">Quận</option>';
                        var ward = '<option value="0">Phường</option>';
                    }
                    else if (result.ward == null) {
                        var provind = '<option value="' + result.provind.ID + '">' + result.provind.Name + '</option>';
                        var district = '<option value="' + result.district.ID + '">' + result.district.Name + '</option>';
                        var ward = '<option value="0">Phường</option>';
                    }
                    $("#Provind1").html(provind);
                    $("#Provind1").css("background", "#f3f4f6");
                    $("#District1").html(district);
                    $("#District1").css("background", "#f3f4f6");
                    $('#Ward1').html(ward);
                    $("#Ward1").css("background", "#f3f4f6");
                }
            });
        } else {
            $.ajax({
                type: 'GET',
                url: '/Order/GetAllProvind',
                success: function (result) {
                    var s = '<option value="0">Thành phố</option>';
                    for (var i = 0; i < result.length; i++) {
                        s += '<option value="' + result[i].ID + '">' + result[i].Name + '</option>';
                    }
                    $('#Provind1').html(s);
                }
            });
            $(this).attr('data-check', "avaiable");
            $("#AddressDetail1").val("");
            $("#AddressDetail1").css("background", "none");
            var district = '<option value="0">Quận</option>';
            var ward = '<option value="0">Phường</option>';
            $("#Provind1").css("background", "none");
            $("#District1").html(district);
            $("#District1").css("background", "none");
            $('#Ward1').html(ward);
            $("#Ward1").css("background", "none");
        }

    });

    $("#userCheck").off('click').on('click', function () {
        var data = $(this).attr('data-user');

        if (data == "check") {
            $(this).attr('data-user', "");
            var name = $("#NameCheck").val();
            $("#NameCheck1").val(name)
            $("#NameCheck1").attr("readonly", "readonly");
            $("#NameCheck1").css("background", "#f3f4f6");
            var relation = '<option value="1">Bản thân</option>';
            $("#RelationCheck").html(relation);
            $("#RelationCheck").css("background", "#f3f4f6");
        } else {
            $(this).attr('data-user', "check");
            $("#NameCheck1").val("");
            $("#NameCheck1").prop("readonly", false);
            $("#NameCheck1").css("background", "none");
            var relation = '<option value="0">Chọn</option>';
            $("#RelationCheck").html(relation);
            $("#RelationCheck").css("background", "none");
        }
    });

});