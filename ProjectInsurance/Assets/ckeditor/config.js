/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
    CKEDITOR.config.uiColor = '#AADC6E';
    CKEDITOR.config.language = 'vi';
    CKEDITOR.config.syntaxhighlight_lang = 'csharp';
    CKEDITOR.config.syntaxhighlight_hideControls = true;
    CKEDITOR.config.filebrowserBrowseUrl = '/Assets/ckfinder/ckfinder.html';
    CKEDITOR.config.filebrowserImageBrowseUrl = '/Assets/ckfinder/ckfinder.html?Type=Images';
    CKEDITOR.config.filebrowserFlashBrowseUrl = '/Assets/ckfinder/ckfinder.html?Type=Flash';
    CKEDITOR.config.filebrowserUploadUrl = '/Assets/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files';
    CKEDITOR.config.filebrowserImageUploadUrl = '/Data';
    CKEDITOR.config.filebrowserFlashUploadUrl = '/Assets/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash';
    CKEDITOR.config.entities_processNumerical = 'force';
     CKFinder.setupCKEditor(null, '/Assets/ckfinder/');
};
