﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectInsurance.Common
{
    public class CommonConstants
    {
        public static string USER_SESSION = null;
        public static string ADMIN_SESSION = null;
        public static string CartSession = "CartSession";
        public static string ORDER_SESSION = null;
    }
}