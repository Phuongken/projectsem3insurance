﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectInsurance
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "News Insurance",
               url: "tin-tuc/{id}",
               defaults: new { controller = "News", action = "News", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Controllers" }
           );

            routes.MapRoute(
               name: "History transaction",
               url: "history-transaction",
               defaults: new { controller = "Home", action = "HistoryContract", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Controllers" }
           );

            routes.MapRoute(
               name: "Contract user",
               url: "contract-user",
               defaults: new { controller = "Home", action = "Contract", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Controllers" }
           );

            routes.MapRoute(
              name: "Charge user",
              url: "Charge",
              defaults: new { controller = "Home", action = "Charge", id = UrlParameter.Optional },
              namespaces: new[] { "ProjectInsurance.Controllers" }
          );

            routes.MapRoute(
               name: "New Insurance",
               url: "tin-moi/{id}",
               defaults: new { controller = "News", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Controllers" }
           );

            routes.MapRoute(
               name: "Insurance",
               url: "baohiem/{id}",
               defaults: new { controller = "Insurance", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Controllers" }
           );

            routes.MapRoute(
               name: "CheckOut",
               url: "thanh-toan-bao-hiem",
               defaults: new { controller = "Checkout", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Controllers" }
           );

            routes.MapRoute(
               name: "Detail Insurance",
               url: "chi-tiet-bao-hiem/{id}",
               defaults: new { controller = "Insurance", action = "Detail", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Controllers" }
           );

            routes.MapRoute(
               name: "Register Insurance",
               url: "dang-ky-bao-hiem/{id}",
               defaults: new { controller = "Order", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Controllers" }
           );


            routes.MapRoute(
               name: "Review Insurance",
               url: "xem-lai-dang-ky",
               defaults: new { controller = "Order", action = "Review", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Controllers" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Controllers" }
            );
        }
    }
}
