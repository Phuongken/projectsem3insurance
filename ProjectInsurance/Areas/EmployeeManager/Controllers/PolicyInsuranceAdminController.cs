﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;

namespace ProjectInsurance.Areas.EmployeeManager.Controllers
{
    public class PolicyInsuranceAdminController : BaseController
    {
        // GET: AdminManager/PolicyInsurance
        public ActionResult Index(int? id)
        {
            var dao = new PolicyInsuranceDao();
            var model = dao.Index(id);
            var daoCate = new PolicyInsuranceCategoryDao();
            ViewBag.PolicyInsuranceCateID = daoCate.Index();
            Session["InsuranceID"] = id;
            return View(model);
        }

        // GET: AdminManager/PolicyInsurance/Details/5
        public ActionResult Details(int? id)
        {
            DataModel db = new DataModel();
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            PolicyInsurance policyInsurance = db.PolicyInsurances.Find(id);
            if (policyInsurance == null)
                return HttpNotFound();
            return View(policyInsurance);
        }

        // GET: AdminManager/PolicyInsurance/Create
        public ActionResult Create(int? id)
        {
            ViewBag.idInsurance = id;
            var daoCate = new PolicyInsuranceCategoryDao();
            ViewBag.PolicyInsuranceCateid = daoCate.Index();
            return View();
        }

        // POST: AdminManager/PolicyInsurance/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(PolicyInsurance newPolicyInsurance, int id)
        {
            if (ModelState.IsValid)
            {
                var dao = new PolicyInsuranceDao();
                newPolicyInsurance.CreateDate = DateTime.Now;
                newPolicyInsurance.UpdateDate = DateTime.Now;
                newPolicyInsurance.CreateBy = 1;
                newPolicyInsurance.UpdateBy = 1;
                newPolicyInsurance.Status = 1;
                bool policyInsurance = dao.Insert(newPolicyInsurance, id);
                if (policyInsurance == true)
                {
                    return Redirect("/AdminManager/PolicyInsuranceAdmin/Index/" + id);
                }
            }
            ViewBag.idInsurance = id;

            var daoCate = new PolicyInsuranceCategoryDao();
            ViewBag.PolicyInsuranceCateid = daoCate.Index();
            return View();
        }

        // GET: AdminManager/PolicyInsurance/Edit/5
        public ActionResult Edit(int? id)
        {
            var policyInsurance = new PolicyInsuranceDao().find(id);
            var daoCate = new PolicyInsuranceCategoryDao();
            ViewBag.PolicyInsuranceCateid = daoCate.Index();
            return View(policyInsurance);
        }

        // POST: AdminManager/PolicyInsurance/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(PolicyInsurance policyInsurance)
        {
            if (ModelState.IsValid)
            {
                var dao = new PolicyInsuranceDao();
                var result = dao.Update(policyInsurance);
                if (result == true)
                {
                    return Redirect("/AdminManager/PolicyInsuranceAdmin/Index/" + Session["InsuranceID"].ToString());
                }
                else
                {
                    ModelState.AddModelError("", "Chỉnh sửa thông tin thất bại");
                }
            }
            return View(policyInsurance);

        }

        // Delete: AdminManager/PolicyInsurance/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            PolicyInsuranceDao policyInsurance = new PolicyInsuranceDao();
            var result = policyInsurance.Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }

        }
    }
}
