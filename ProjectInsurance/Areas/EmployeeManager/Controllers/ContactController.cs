﻿using Model.Dao;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Areas.EmployeeManager.Controllers
{
    public class ContactController : Controller
    {
        // GET: EmployeeManager/Contact
        public ActionResult Index()
        {
            ContactDao dao = new ContactDao();
            var contact = dao.ListContact();
            return View(contact);

        }


        // Get: AdminManager/Contact/Detail/6
        public ActionResult Details(int? id)
        {
            DataModel db = new DataModel();
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
                return HttpNotFound();
            return View(contact);


        }

        public JsonResult Delete(int? id)
        {
            var dao = new ContactDao();
            var result = dao.Delete(id);
            if (result == true)
            {

                return Json(new
                {
                    id = true
                });
            }
            else
            {
                return Json(new
                {
                    id = false
                });
            }
        }
    }
}