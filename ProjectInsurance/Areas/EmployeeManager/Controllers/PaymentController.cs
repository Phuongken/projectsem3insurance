﻿using Model.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Areas.EmployeeManager.Controllers
{
    public class PaymentController : BaseController
    {
        // GET: EmployeeManager/Payment
        public ActionResult Index()
        {
            var dao = new PaymentDao();
            var result = dao.ListPayment();
            return View(result);
        }

        public JsonResult ChangeStatusDone(int? id)
        {
            var dao = new PaymentDao();
            var result = dao.changeStatusDone(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }


        }
        public JsonResult ChangeStatusCancel(int? id)
        {
            var dao = new PaymentDao();
            var result = dao.changeStatusCancel(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }


        }
    }
}