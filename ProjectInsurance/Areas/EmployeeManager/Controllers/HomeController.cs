﻿using Model.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Areas.EmployeeManager.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            var cateInsurance = new InsuranceCategoryDao();
            var cateNew = new NewCategoryDao();
            Session["cateNewID"] = cateNew.ViewBagNew();
            Session["cateInsuranceID"] = cateInsurance.viewbagIndex();
            return View();
        }
    }
}