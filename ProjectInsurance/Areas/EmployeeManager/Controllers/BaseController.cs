﻿using ProjectInsurance.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Areas.EmployeeManager.Controllers
{
    public class BaseController : Controller
    {
        // GET: EmployeeManager/Base
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var session = (EmpLogin)Session["EmployeeSession"];
            if (session == null)
            {
                filterContext.Result = new RedirectToRouteResult(new
                    System.Web.Routing.RouteValueDictionary(new { controller = "Login", action = "Index", Area = "EmployeeManager" }));
            }
            base.OnActionExecuting(filterContext);
        }
    }
}