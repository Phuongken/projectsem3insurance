﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;

namespace ProjectInsurance.Areas.EmployeeManager.Controllers
{
    public class InsuranceAdminController : BaseController
    {

        // GET: AdminManager/Insurance/Details/5
        public ActionResult Details(int? id)
        {
            DataModel db = new DataModel();
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Insurance insurance = db.Insurances.Find(id);
            if (insurance == null)
                return HttpNotFound();
            var daoCate = new InsuranceDao();
            ViewBag.InsuranceID = daoCate.Index();
            var daoPolicy = new PolicyInsuranceDao();
            ViewBag.PolicyInsuranceID = daoPolicy.QuyenLoiChinh(id);
            //
            var Uudiem = new PolicyInsuranceDao();
            ViewBag.PolicyInsuranceIDUuDiem = daoPolicy.UuDiem(id);
            //
            var ThoiGianCho = new PolicyInsuranceDao();
            ViewBag.PolicyInsuranceIDThoiGianCho = daoPolicy.ThoiGianCho(id);
            //
            var QuyDinhDongChiTra = new PolicyInsuranceDao();
            ViewBag.PolicyInsuranceIDQuyDinhDongChiTra = daoPolicy.QuyDinhDongChiTra(id);
            //
            var LoaiTru = new PolicyInsuranceDao();
            ViewBag.PolicyInsuranceIDLoaiTru = daoPolicy.LoaiTru(id);
            //
            var DoiTuongThamGia = new PolicyInsuranceDao();
            ViewBag.PolicyInsuranceIDDoiTuongThamGia = daoPolicy.DoiTuongThamGia(id);
            //
            var QuyTac = new PolicyInsuranceDao();
            ViewBag.PolicyInsuranceIDQuyTac = daoPolicy.QuyTac(id);
            //
            var ChuongTrinh = new PolicyInsuranceDao();
            ViewBag.PolicyInsuranceIDChuongTrinh = daoPolicy.DoiTuongThamGia(id);
            return View(insurance);
        }

        // GET: AdminManager/Insurance/Create
        [HttpGet]
        public ActionResult Create(int id)
        {
            ViewBag.InsuranceID = id;
            return View();
        }

        // POST: AdminManager/Insurance/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(Insurance newInsurance, int id)
        {
            if (ModelState.IsValid)
            {
                var dao = new InsuranceDao();
                newInsurance.CreateDate = DateTime.Now;
                newInsurance.UpdateDate = DateTime.Now;
                newInsurance.CreateBy = 1;
                newInsurance.UpdateBy = 1;
                newInsurance.Status = 1;
                newInsurance.InsuranceCategoryID = id;
                bool insurance = dao.Insert(newInsurance);
                if (insurance == true)
                {
                    return Redirect("/chi-tiet/" + id);
                }
            }
            return View(newInsurance);
        }

        // GET: AdminManager/Insurance/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            InsuranceDao i = new InsuranceDao();
            var insurance = i.find(id);
            return View(insurance);
        }

        // POST: AdminManager/Insurance/Edit/5
        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[ValidateInput(false)]
        public ActionResult Edit(Insurance insurance, int id)
        {
            if (ModelState.IsValid)
            {
                var dao = new InsuranceDao();
                insurance.InsuranceCategoryID = id;
                var result = dao.Update(insurance);
                if (result == true)
                {
                    return Redirect("/chi-tiet/" + Session["CategoryID"].ToString());
                }
                else
                {
                    ModelState.AddModelError("", "Chỉnh sửa thông tin thất bại");
                }
            }
            return View(insurance);
        }

        // POST: AdminManager/Insurance/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            InsuranceDao insurance = new InsuranceDao();
            var result = insurance.Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
        public ActionResult InsuranceCategory(int id)
        {
            var category = new InsuranceCategoryDao();
            var result = category.Index(id);
            ViewBag.InsuranceID = id;
            //ViewBag.cateInsurance = category.viewbagIndex();
            Session["CategoryID"] = id;
            return View(result);
        }

    }
}
