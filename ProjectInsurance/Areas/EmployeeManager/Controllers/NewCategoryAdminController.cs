﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;

namespace ProjectInsurance.Areas.EmployeeManager.Controllers
{
    public class NewCategoryAdminController : BaseController
    {
        // GET: AdminManager/NewCategoryAdmin
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AdminManager/InsuranceCategory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminManager/InsuranceCategory/Create
        [HttpPost]
        public ActionResult Create(CategoryNew newCategoryNew)
        {
            if (ModelState.IsValid)
            {
                var dao = new NewCategoryDao();
                newCategoryNew.Status = 1;
                bool result = dao.Insert(newCategoryNew);
                if (result == true)
                {
                    return RedirectToAction("Index", "NewCategoryAdmin");
                }
            }
            return View(newCategoryNew);
        }

        // GET: AdminManager/InsuranceCategory/Edit/5
        public ActionResult Edit(int? id)
        {
            NewCategoryDao dao = new NewCategoryDao();
            var newCategory = dao.find(id);
            return View(newCategory);
        }

        // POST: AdminManager/InsuranceCategory/Edit/5
        [HttpPost]
        public ActionResult Edit(CategoryNew categoryNew)
        {
            if (ModelState.IsValid)
            {
                var dao = new NewCategoryDao();
                var result = dao.Update(categoryNew);
                if (result == true)
                {
                    return RedirectToAction("Index", "NewCategoryAdmin");
                }
                else
                {
                    ModelState.AddModelError("", "Chỉnh sửa thông tin thất bại");
                }
            }
            return View(categoryNew);
        }

        // POST: AdminManager/InsuranceCategory/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            NewCategoryDao newCategory = new NewCategoryDao();
            var result = newCategory.Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
    }
}
