﻿using Model.Dao;
using ProjectInsurance.Areas.EmployeeManager.Models;
using ProjectInsurance.Common;
using System.Web.Mvc;

namespace ProjectInsurance.Areas.EmployeeManager.Controllers
{
    public class LoginController : Controller
    {
        // GET: EmployeeManager/Login
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login(LoginModelEmployee employee)
        {
            if (ModelState.IsValid)
            {
                var dao = new EmployeeDao();
                var result = dao.Login(employee.Username, employee.Password);
                if (result == true)
                {
                    var cateInsurance = new InsuranceCategoryDao();
                    var cateNew = new NewCategoryDao();
                    var empLogin = dao.GetById(employee.Username);
                    var empSession = new EmpLogin();
                    empSession.UserID = empLogin.ID;
                    empSession.UserName = empLogin.Username;
                    Session["cateNewID"] = cateNew.ViewBagNew();
                    Session["cateInsuranceID"] = cateInsurance.viewbagIndex();
                    Session["EmployeeSession"] = empSession;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Tài khoản hoặc mật khẩu chưa chính xác");
                }

            }
            return View("Index");
        }
    }
}