﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;
using ProjectInsurance.Models;

namespace ProjectInsurance.Areas.EmployeeManager.Controllers
{
    public class NewAdminController : BaseController
    {
        // GET: AdminManager/NewAdmin

        NewDao dao = new NewDao();
        // GET: AdminManager/NewAdmin/Details/5
        public ActionResult Details(int id)
        {
            DataModel db = new DataModel();
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            New new1 = db.News.Find(id);
            if (new1 == null)
                return HttpNotFound();
            return View(new1);
        }

        // GET: AdminManager/NewAdmin/Create
        public ActionResult Create(int id)
        {
            ViewBag.NewID = id;
            return View();
        }

        // POST: AdminManager/NewAdmin/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(New newNew1, int id)
        {
            if (ModelState.IsValid)
            {
                newNew1.CreateDate = DateTime.Now;
                newNew1.UpdateDate = DateTime.Now;
                newNew1.CreateBy = 1;
                newNew1.UpdateBy = 1;
                newNew1.Status = 1;
                newNew1.CategoryID = id;
                bool new1 = dao.Insert(newNew1);
                if (new1 == true)
                {
                    return Redirect("/de-an-chien-luoc/" + id);
                }
            }
            ViewBag.NewID = id;
            return View(newNew1);
        }

        // GET: AdminManager/NewAdmin/Edit/5
        public ActionResult Edit(int id)
        {
            var result = dao.find(id);
            return View(result);
        }

        // POST: AdminManager/NewAdmin/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(New new1, int id)
        {
            if (ModelState.IsValid)
            {
                new1.CategoryID = id;
                var result = dao.Update(new1);
                if (result == true)
                {
                    return Redirect("/de-an-chien-luoc/" + Session["CategoryNewID"].ToString());
                }
                else
                {
                    ModelState.AddModelError("", "Chỉnh sửa thông tin thất bại");
                }
            }
            return View(new1);
        }

        // POST: AdminManager/NewAdmin/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            var result = dao.Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
        public ActionResult NewCate(int id)
        {
            var category = new NewDao();
            var result = category.Index(id);
            ViewBag.NewID = id;
            Session["CategoryNewID"] = id;
            return View(result);
        }
        public ActionResult ViewMail()
        {
            return View();
        }
        public ActionResult SendMailInfo(int id)
        {
            SendMail sendMail = new SendMail();
            var dao = new NewDao();
            var result = dao.getInfoOnline();
            var txtNew = dao.Find(id);

            try
            {
                //String renderedHTML = Controllers.MailAdminController.RenderViewToString("NewAdmin", "ViewMail", txtNew); 
                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/NewAdmin/ViewMail.cshtml"));
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{Name}}", txtNew.Name);
                content = content.Replace("{{Title}}", txtNew.Title);
                foreach (var item in result)
                {
                    sendMail.SendEmail(item.Email, "Bản tin mới từ insurance team", content);
                }

                Session["SendMailSuccsess"] = 1;

                return Redirect("/de-an-chien-luoc/" + Session["CategoryNewID"].ToString());
            }
            catch (Exception ex)
            {
                Session["SendMailError"] = 1;

                return Redirect("/de-an-chien-luoc/" + Session["CategoryNewID"].ToString());
            }


        }

        public JsonResult ChangeSession()
        {
            Session["SendMailSuccsess"] = null;
            Session["SendMailError"] = null;
            return Json(new
            {
                status = true
            });
        }


    }
}
