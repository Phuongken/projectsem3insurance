﻿using Model.Dao;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Areas.EmployeeManager.Controllers
{
    public class InfoOnlineController : BaseController
    {
        // GET: AdminManager/InfoOnline
        public ActionResult Index()
        {
            InfoOnlineDao dao = new InfoOnlineDao();
            var infoOnline = dao.ListInfoOnline();
            return View(infoOnline);
        }
        // Get: AdminManager/Contact/Detail/7
        public ActionResult Detail(int? id)
        {
            DataModel db = new DataModel();
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            InfoOnline infoOnline = db.InfoOnlines.Find(id);
            if (infoOnline == null)
                return HttpNotFound();
            return View(infoOnline);

        }
        public JsonResult Delete(int? id)
        {
            var dao = new InfoOnlineDao();
            var result = dao.Delete(id);
            if (result == true)
            {

                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
    }
}