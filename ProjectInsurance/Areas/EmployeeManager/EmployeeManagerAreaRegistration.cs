﻿using System.Web.Mvc;

namespace ProjectInsurance.Areas.EmployeeManager
{
    public class EmployeeManagerAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "EmployeeManager";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
               "Home Employee",
               "trang-chu-employee",
               new { controller = "Home", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Areas.EmployeeManager.Controllers" }
           );


            context.MapRoute(
               "List user a",
               "danh-sach-khach-hang",
               new { controller = "User",action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Areas.EmployeeManager.Controllers" }
           );

            context.MapRoute(
               "contract employee",
               "hop-dong-employee",
               new { controller = "ContractAdmin", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Areas.EmployeeManager.Controllers" }
           );

            context.MapRoute(
                name: "New Category e",
                url: "de-an-chien-luoc-employee/{id}",
                defaults: new { controller = "NewAdmin", action = "NewCate", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Areas.EmployeeManager.Controllers" }
            );

            context.MapRoute(
                name: "Insurance Category e",
                url: "chi-tiet-employee/{id}",
                defaults: new { controller = "InsuranceAdmin", action = "InsuranceCategory", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Areas.EmployeeManager.Controllers" }
            );

            context.MapRoute(
                name: "Logout empoyee",
                url: "logout",
                defaults: new { controller = "Login", action = "LogOut", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Areas.EmployeeManager.Controllers" }
            );

            context.MapRoute(
               name: "Login admin e",
               url: "employee/login",
               defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Areas.EmployeeManager.Controllers" }
           );

            context.MapRoute(
               "EmployeeManager_default",
               "EmployeeManager/{controller}/{action}/{id}",
               new { action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Areas.EmployeeManager.Controllers" }
           );
        }
    }
}