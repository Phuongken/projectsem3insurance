﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;
using ProjectInsurance.Areas.AdminManager.Models;

namespace ProjectInsurance.Areas.AdminManager.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            var cateInsurance = new InsuranceCategoryDao();
            var cateNew = new NewCategoryDao();
            Session["cateNewID"] = cateNew.ViewBagNew();
            Session["cateInsuranceID"] = cateInsurance.viewbagIndex();
            return View();
        }
    }
}