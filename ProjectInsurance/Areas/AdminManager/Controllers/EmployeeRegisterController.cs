﻿using Model.Dao;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Areas.AdminManager.Controllers
{
    public class EmployeeRegisterController : BaseController
    {
        [HttpGet]
        // GET: AdminManager/EmployeeRegister
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(Employee emp)
        {
            if (ModelState.IsValid)
            {
                var dao = new EmployeeDao();
                var result = dao.Register(emp);
                if (result == true)
                {
                    Session["RegisterSuccess"] = 1;
                    return RedirectToAction("Index", "EmployeeRegister");
                }
                else
                {
                    Session["RegisterFail"] = 1;
                    return View(emp);
                }
            }
            return View(emp);
        }

        public JsonResult ChangeSession()
        {
            Session["RegisterSuccess"] = null;
            Session["RegisterFail"] = null;

            return Json(new
            {
                status = true
            });

        }
    }
}