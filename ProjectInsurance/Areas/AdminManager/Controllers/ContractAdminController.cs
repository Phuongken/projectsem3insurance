﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;
using ProjectInsurance.Common;
using ProjectInsurance.Models;

namespace ProjectInsurance.Areas.AdminManager.Controllers
{
    public class ContractAdminController : BaseController
    {
        DataModel db = new DataModel();
        ContractDao dao = new ContractDao();
        // GET: AdminManager/ContractAdmin
        public ActionResult Index()
        {
            var model = dao.Index();
            //ViewBag.getInforUser = dao.getInfoUser();
            ViewBag.getInforUser = dao.getOrderDetail();
            return View(model);
        }
        public ActionResult Details(int id)
        {
            DataModel db = new DataModel();
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Contract contract1 = db.Contracts.Find(id);
            if (contract1 == null)
                return HttpNotFound();
            ViewBag.getOrderDetail = dao.getOrderDetails(id);
            return View(contract1);
        }
        // GET: AdminManager/ContractAdmin/Edit/5
        public ActionResult Edit(int? id)
        {
            var model = dao.Find(id);
            return View(model);
        }
        //
        [HttpPost]
        public ActionResult Edit(Contract contract, int id, OrderDetail orderDetail)
        {
            SendMail sendMail = new SendMail();
            var dao = new ContractDao();
            var result1 = dao.getOrderDetail(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var txtNew = dao.Find(id);
            var result = dao.Update(contract, Convert.ToInt32(adminID));

            if (result == true)
            {
                bool history = dao.InsertHistoryContract(id);
                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ContractAdmin/ViewMailEdit.cshtml"));
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{ContractName}}", txtNew.ContractName);
                content = content.Replace("{{UserID}}", txtNew.UserID.ToString());
                content = content.Replace("{{TotalPrice}}", txtNew.TotalPrice.ToString());
                content = content.Replace("{{StartDate}}", txtNew.StartDate.ToString());
                content = content.Replace("{{EndDate}}", txtNew.EndDate.ToString());
                content = content.Replace("{{PaymentAddress}}", txtNew.PaymentAddress);
                content = content.Replace("{{DeliveryAddress}}", txtNew.DeliveryAddress);
                if (txtNew.Status == 2)
                {
                    content = content.Replace("{{Status}}", "Đang xử lý");
                }
                else if (txtNew.Status == 1)
                {
                    content = content.Replace("{{Status}}", "Hoạt động");
                }
                else
                {
                    content = content.Replace("{{Status}}", "Khóa");
                }

                //orderDetail
                content = content.Replace("{{InsuranceID}}", getOrderDetail.InsurantID.ToString());
                content = content.Replace("{{ContractID}}", getOrderDetail.ContractID.ToString());
                content = content.Replace("{{Quantity}}", getOrderDetail.Quantity.ToString());
                content = content.Replace("{{Price}}", getOrderDetail.Price.ToString());
                content = content.Replace("{{StartDate}}", getOrderDetail.StartDate.ToString());
                content = content.Replace("{{EndDate}}", getOrderDetail.EndDate.ToString());
                content = content.Replace("{{FullName}}", getOrderDetail.FullName.ToString());
                content = content.Replace("{{HomePhone}}", getOrderDetail.HomePhone.ToString());
                content = content.Replace("{{MobilePhone}}", getOrderDetail.MobilePhone.ToString());
                content = content.Replace("{{Email}}", getOrderDetail.Email.ToString());
                content = content.Replace("{{AddressDetail}}", getOrderDetail.AddressDetail.ToString());
                content = content.Replace("{{AddressID}}", getOrderDetail.AddressID.ToString());
                content = content.Replace("{{AddressContract}}", getOrderDetail.AddressContract.ToString());
                content = content.Replace("{{AddressIDContract}}", getOrderDetail.AddressIDContract.ToString());
                content = content.Replace("{{FullNameReceive}}", getOrderDetail.FullNameReceive.ToString());
                content = content.Replace("{{Dob}}", getOrderDetail.Dob.ToString());
                content = content.Replace("{{Identity}}", getOrderDetail.Identity.ToString());
                if (getOrderDetail.Relationship == 1)
                {
                    content = content.Replace("{{Relationship}}", "Your self");
                }
                else if (getOrderDetail.Relationship == 2)
                {
                    content = content.Replace("{{Relationship}}", "Father");
                }
                else if (getOrderDetail.Relationship == 3)
                {
                    content = content.Replace("{{Relationship}}", "Mother");
                }
                else if (getOrderDetail.Relationship == 4)
                {
                    content = content.Replace("{{Relationship}}", "Wife");
                }
                else if (getOrderDetail.Relationship == 5)
                {
                    content = content.Replace("{{Relationship}}", "Husband");
                }
                else if (getOrderDetail.Relationship == 6)
                {
                    content = content.Replace("{{Relationship}}", "Child");
                }
                if (getOrderDetail.Gender == 1)
                {
                    content = content.Replace("{{Gender}}", "Nam");
                }
                else
                {
                    content = content.Replace("{{Gender}}", "Nữ");
                }
                content = content.Replace("{{Job}}", getOrderDetail.Job.ToString());
                content = content.Replace("{{TermInsurance}}", getOrderDetail.TermInsurance.ToString());
                content = content.Replace("{{HousePrice}}", getOrderDetail.HousePrice.ToString());
                sendMail.SendEmail(result1, "Bản tin mới từ insurance team", content);
                Session["SendMailSuccsess"] = 1;
                return RedirectToAction("Index", "ContractAdmin");
            }
            else
            {
                ModelState.AddModelError("", "Chỉnh sửa hợp đồng thất bại.");
            }

            return View(contract);
        }
        // edit OrderDetail
        public ActionResult EditOrderDetail(int? id)
        {
            DataModel db = new DataModel();
            var model = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            //var model = dao.Find(id);
            return View(model);
        }
        //
        [HttpPost]
        public ActionResult EditOrderDetail(OrderDetail orderDetail, int id)
        {
            SendMail sendMail = new SendMail();
            var dao = new ContractDao();
            var result1 = dao.getOrderDetail(id);
            var txtNew = dao.Find(id);
            var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var result = dao.UpdateOrderDetail(orderDetail);
            if (result == true)
            {
                bool history = dao.InsertHistoryContract(id);
                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ContractAdmin/ViewMailEdit.cshtml"));
                //contract
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{ContractName}}", txtNew.ContractName);
                content = content.Replace("{{UserID}}", txtNew.UserID.ToString());
                content = content.Replace("{{TotalPrice}}", txtNew.TotalPrice.ToString());
                content = content.Replace("{{StartDate}}", txtNew.StartDate.ToString());
                content = content.Replace("{{EndDate}}", txtNew.EndDate.ToString());
                content = content.Replace("{{PaymentAddress}}", txtNew.PaymentAddress);
                content = content.Replace("{{DeliveryAddress}}", txtNew.DeliveryAddress);
                if (txtNew.Status == 2)
                {
                    content = content.Replace("{{Status}}", "Đang xử lý");
                }
                else if (txtNew.Status == 1)
                {
                    content = content.Replace("{{Status}}", "Hoạt động");
                }
                else
                {
                    content = content.Replace("{{Status}}", "Khóa");
                }
                //orderDetail
                content = content.Replace("{{InsuranceID}}", getOrderDetail.InsurantID.ToString());
                content = content.Replace("{{ContractID}}", getOrderDetail.ContractID.ToString());
                content = content.Replace("{{Quantity}}", getOrderDetail.Quantity.ToString());
                content = content.Replace("{{Price}}", getOrderDetail.Price.ToString());
                content = content.Replace("{{StartDate}}", getOrderDetail.StartDate.ToString());
                content = content.Replace("{{EndDate}}", getOrderDetail.EndDate.ToString());
                content = content.Replace("{{FullName}}", getOrderDetail.FullName.ToString());
                content = content.Replace("{{HomePhone}}", getOrderDetail.HomePhone.ToString());
                content = content.Replace("{{MobilePhone}}", getOrderDetail.MobilePhone.ToString());
                content = content.Replace("{{Email}}", getOrderDetail.Email.ToString());
                content = content.Replace("{{AddressDetail}}", getOrderDetail.AddressDetail.ToString());
                content = content.Replace("{{AddressID}}", getOrderDetail.AddressID.ToString());
                content = content.Replace("{{AddressContract}}", getOrderDetail.AddressContract.ToString());
                content = content.Replace("{{AddressIDContract}}", getOrderDetail.AddressIDContract.ToString());
                content = content.Replace("{{FullNameReceive}}", getOrderDetail.FullNameReceive.ToString());
                content = content.Replace("{{Dob}}", getOrderDetail.Dob.ToString());
                content = content.Replace("{{Identity}}", getOrderDetail.Identity.ToString());
                if (getOrderDetail.Relationship == 1)
                {
                    content = content.Replace("{{Relationship}}", "Your self");
                }
                else if (getOrderDetail.Relationship == 2)
                {
                    content = content.Replace("{{Relationship}}", "Father");
                }
                else if (getOrderDetail.Relationship == 3)
                {
                    content = content.Replace("{{Relationship}}", "Mother");
                }
                else if (getOrderDetail.Relationship == 4)
                {
                    content = content.Replace("{{Relationship}}", "Wife");
                }
                else if (getOrderDetail.Relationship == 5)
                {
                    content = content.Replace("{{Relationship}}", "Husband");
                }
                else if (getOrderDetail.Relationship == 6)
                {
                    content = content.Replace("{{Relationship}}", "Child");
                }
                if (getOrderDetail.Gender == 1)
                {
                    content = content.Replace("{{Gender}}", "Nam");
                }
                else
                {
                    content = content.Replace("{{Gender}}", "Nữ");
                }
                content = content.Replace("{{Job}}", getOrderDetail.Job.ToString());
                content = content.Replace("{{TermInsurance}}", getOrderDetail.TermInsurance.ToString());
                content = content.Replace("{{HousePrice}}", getOrderDetail.HousePrice.ToString());

                sendMail.SendEmail(result1, "Bản tin mới từ insurance team", content);

                Session["SendMailSuccsess"] = 1;
                return RedirectToAction("Index", "ContractAdmin");
            }
            else
            {
                ModelState.AddModelError("", "Chỉnh sửa hợp đồng thất bại.");
            }

            return View(result);
        }
        //
        public ActionResult reOpen(int? id)
        {
            var model = dao.Find(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult reOpen(Contract contract, int id)
        {
            SendMail sendMail = new SendMail();
            var dao = new ContractDao();
            var result1 = dao.getOrderDetail(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            var txtNew = dao.Find(id);
            var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var result = dao.Update(contract, Convert.ToInt32(adminID));
            if (result == true)
            {
                bool history = dao.InsertHistoryContract(id);
                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ContractAdmin/ViewMailReOpen.cshtml"));
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{ContractName}}", txtNew.ContractName);
                content = content.Replace("{{UserID}}", txtNew.UserID.ToString());
                content = content.Replace("{{TotalPrice}}", txtNew.TotalPrice.ToString());
                content = content.Replace("{{StartDate}}", txtNew.StartDate.ToString());
                content = content.Replace("{{EndDate}}", txtNew.EndDate.ToString());
                content = content.Replace("{{PaymentAddress}}", txtNew.PaymentAddress);
                content = content.Replace("{{DeliveryAddress}}", txtNew.DeliveryAddress);
                if (txtNew.Status == 2)
                {
                    content = content.Replace("{{Status}}", "Đang xử lý");
                }
                else if (txtNew.Status == 1)
                {
                    content = content.Replace("{{Status}}", "Hoạt động");
                }
                else
                {
                    content = content.Replace("{{Status}}", "Khóa");
                }

                //orderDetail
                content = content.Replace("{{InsuranceID}}", getOrderDetail.InsurantID.ToString());
                content = content.Replace("{{ContractID}}", getOrderDetail.ContractID.ToString());
                content = content.Replace("{{Quantity}}", getOrderDetail.Quantity.ToString());
                content = content.Replace("{{Price}}", getOrderDetail.Price.ToString());
                content = content.Replace("{{StartDate}}", getOrderDetail.StartDate.ToString());
                content = content.Replace("{{EndDate}}", getOrderDetail.EndDate.ToString());
                content = content.Replace("{{FullName}}", getOrderDetail.FullName.ToString());
                content = content.Replace("{{HomePhone}}", getOrderDetail.HomePhone.ToString());
                content = content.Replace("{{MobilePhone}}", getOrderDetail.MobilePhone.ToString());
                content = content.Replace("{{Email}}", getOrderDetail.Email.ToString());
                content = content.Replace("{{AddressDetail}}", getOrderDetail.AddressDetail.ToString());
                content = content.Replace("{{AddressID}}", getOrderDetail.AddressID.ToString());
                content = content.Replace("{{AddressContract}}", getOrderDetail.AddressContract.ToString());
                content = content.Replace("{{AddressIDContract}}", getOrderDetail.AddressIDContract.ToString());
                content = content.Replace("{{FullNameReceive}}", getOrderDetail.FullNameReceive.ToString());
                content = content.Replace("{{Dob}}", getOrderDetail.Dob.ToString());
                content = content.Replace("{{Identity}}", getOrderDetail.Identity.ToString());
                if (getOrderDetail.Relationship == 1)
                {
                    content = content.Replace("{{Relationship}}", "Your self");
                }
                else if (getOrderDetail.Relationship == 2)
                {
                    content = content.Replace("{{Relationship}}", "Father");
                }
                else if (getOrderDetail.Relationship == 3)
                {
                    content = content.Replace("{{Relationship}}", "Mother");
                }
                else if (getOrderDetail.Relationship == 4)
                {
                    content = content.Replace("{{Relationship}}", "Wife");
                }
                else if (getOrderDetail.Relationship == 5)
                {
                    content = content.Replace("{{Relationship}}", "Husband");
                }
                else if (getOrderDetail.Relationship == 6)
                {
                    content = content.Replace("{{Relationship}}", "Child");
                }
                if (getOrderDetail.Gender == 1)
                {
                    content = content.Replace("{{Gender}}", "Nam");
                }
                else
                {
                    content = content.Replace("{{Gender}}", "Nữ");
                }
                content = content.Replace("{{Job}}", getOrderDetail.Job.ToString());
                content = content.Replace("{{TermInsurance}}", getOrderDetail.TermInsurance.ToString());
                content = content.Replace("{{HousePrice}}", getOrderDetail.HousePrice.ToString());

                sendMail.SendEmail(result1, "Bản tin mới từ insurance team", content);

                Session["SendMailSuccsess"] = 1;
                return RedirectToAction("Index", "ContractAdmin");
            }
            else
            {
                ModelState.AddModelError("", "Chỉnh sửa hợp đồng thất bại.");
            }
            return View(contract);
        }
        // POST: AdminManager/ContractAdmin/Edit/5


        // POST: AdminManager/ContractAdmin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            SendMail sendMail = new SendMail();
            var dao = new ContractDao();
            var result1 = dao.getOrderDetail(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            var txtNew = dao.Find(id);
            var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var result = dao.Delete(id, Convert.ToInt32(adminID));
            if (result == true)
            {
                bool history = dao.InsertHistoryContract(id);
                if (history == true)
                {
                    string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ContractAdmin/ViewMailDelete.cshtml"));
                    content = content.Replace("{{ID}}", txtNew.ID.ToString());
                    content = content.Replace("{{ContractName}}", txtNew.ContractName);
                    content = content.Replace("{{UserID}}", txtNew.UserID.ToString());
                    content = content.Replace("{{TotalPrice}}", txtNew.TotalPrice.ToString());
                    content = content.Replace("{{StartDate}}", txtNew.StartDate.ToString());
                    content = content.Replace("{{EndDate}}", txtNew.EndDate.ToString());
                    content = content.Replace("{{PaymentAddress}}", txtNew.PaymentAddress);
                    content = content.Replace("{{DeliveryAddress}}", txtNew.DeliveryAddress);
                    if (txtNew.Status == 2)
                    {
                        content = content.Replace("{{Status}}", "Đang xử lý");
                    }
                    else if (txtNew.Status == 1)
                    {
                        content = content.Replace("{{Status}}", "Hoạt động");
                    }
                    else
                    {
                        content = content.Replace("{{Status}}", "Khóa");
                    }
                    //orderDetail
                    content = content.Replace("{{InsuranceID}}", getOrderDetail.InsurantID.ToString());
                    content = content.Replace("{{ContractID}}", getOrderDetail.ContractID.ToString());
                    content = content.Replace("{{Quantity}}", getOrderDetail.Quantity.ToString());
                    content = content.Replace("{{Price}}", getOrderDetail.Price.ToString());
                    content = content.Replace("{{StartDate}}", getOrderDetail.StartDate.ToString());
                    content = content.Replace("{{EndDate}}", getOrderDetail.EndDate.ToString());
                    content = content.Replace("{{FullName}}", getOrderDetail.FullName.ToString());
                    content = content.Replace("{{HomePhone}}", getOrderDetail.HomePhone.ToString());
                    content = content.Replace("{{MobilePhone}}", getOrderDetail.MobilePhone.ToString());
                    content = content.Replace("{{Email}}", getOrderDetail.Email.ToString());
                    content = content.Replace("{{AddressDetail}}", getOrderDetail.AddressDetail.ToString());
                    content = content.Replace("{{AddressID}}", getOrderDetail.AddressID.ToString());
                    content = content.Replace("{{AddressContract}}", getOrderDetail.AddressContract.ToString());
                    content = content.Replace("{{AddressIDContract}}", getOrderDetail.AddressIDContract.ToString());
                    content = content.Replace("{{FullNameReceive}}", getOrderDetail.FullNameReceive.ToString());
                    content = content.Replace("{{Dob}}", getOrderDetail.Dob.ToString());
                    content = content.Replace("{{Identity}}", getOrderDetail.Identity.ToString());
                    if (getOrderDetail.Relationship == 1)
                    {
                        content = content.Replace("{{Relationship}}", "Your self");
                    }
                    else if (getOrderDetail.Relationship == 2)
                    {
                        content = content.Replace("{{Relationship}}", "Father");
                    }
                    else if (getOrderDetail.Relationship == 3)
                    {
                        content = content.Replace("{{Relationship}}", "Mother");
                    }
                    else if (getOrderDetail.Relationship == 4)
                    {
                        content = content.Replace("{{Relationship}}", "Wife");
                    }
                    else if (getOrderDetail.Relationship == 5)
                    {
                        content = content.Replace("{{Relationship}}", "Husband");
                    }
                    else if (getOrderDetail.Relationship == 6)
                    {
                        content = content.Replace("{{Relationship}}", "Child");
                    }
                    if (getOrderDetail.Gender == 1)
                    {
                        content = content.Replace("{{Gender}}", "Nam");
                    }
                    else
                    {
                        content = content.Replace("{{Gender}}", "Nữ");
                    }
                    content = content.Replace("{{Job}}", getOrderDetail.Job.ToString());
                    content = content.Replace("{{TermInsurance}}", getOrderDetail.TermInsurance.ToString());
                    content = content.Replace("{{HousePrice}}", getOrderDetail.HousePrice.ToString());

                    sendMail.SendEmail(result1, "Bản tin mới từ insurance team", content);

                    Session["SendMailSuccsess"] = 1;
                    return Json(new
                    {
                        status = true
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
        //
        public ActionResult ViewMailDelete()
        {
            return View();
        }
        public ActionResult ViewMailOpenContract()
        {
            return View();
        }
        public ActionResult ViewMailReOpen()
        {
            return View();
        }
        public ActionResult ViewMailActive()
        {
            return View();
        }
        public ActionResult ViewMailEdit()
        {
            return View();
        }
        //
        public ActionResult ViewMail()
        {
            return View();
        }

        public JsonResult Finter(string id, string startDate, string endDate, string search, string idStatus, string idDuration)
        {
            var ID = Convert.ToInt32(id);
            var IDStatus = Convert.ToInt32(idStatus);
            var IDDuration = Convert.ToInt32(idDuration);
            var StartDate = Convert.ToDateTime(startDate);
            var EndDate = Convert.ToDateTime(endDate);

            var emd = "";
            DateTime param = Convert.ToDateTime(DateTime.Now);
            string m = param.ToString("MM");
            string d = param.ToString("dd");
            string y = param.ToString("yyyy");
            int date = Convert.ToInt32(d) + 30;
            if (date > 31)
            {
                int month = Convert.ToInt32(m) + 1;
                emd = month + "/" + d + "/" + y;
            }
            else
            {
                emd = m + "/" + d + "/" + y;
            }

            var Duration = Convert.ToDateTime(emd);
            DataModel db = new DataModel();
            if (ID == 0)
            {
                if (IDStatus == 10)
                {
                    if (IDDuration == 0)
                    {
                        var list = (from c in db.Contracts
                                    join o in db.OrderDetails on c.ID equals o.ContractID
                                    join i in db.Insurances on o.InsurantID equals i.ID
                                    join ci in db.InsuranceCategories on i.InsuranceCategoryID equals ci.ID
                                    join u in db.Users on c.UserID equals u.ID
                                    where (c.CreateDate >= StartDate && c.CreateDate <= EndDate)
                                    && (c.ContractName.Contains(search) || c.PaymentAddress.Contains(search)
                                    || c.DeliveryAddress.Contains(search) || c.Status.ToString().Contains(search)
                                    || c.ID.ToString().Contains(search) || c.UserID.ToString().Contains(search)
                                    || c.UpdateBy.ToString().Contains(search) || c.CreateBy.ToString().Contains(search)
                                    )
                                    select new { Contract = c, User = u, OrderDetail = o }
                                   );
                        return Json(new
                        {
                            value = list.ToList(),
                            JsonRequestBehavior.AllowGet
                        });
                    }
                    else if (IDDuration == 1)
                    {
                        var list = (from c in db.Contracts
                                    join o in db.OrderDetails on c.ID equals o.ContractID
                                    join i in db.Insurances on o.InsurantID equals i.ID
                                    join ci in db.InsuranceCategories on i.InsuranceCategoryID equals ci.ID
                                    join u in db.Users on c.UserID equals u.ID
                                    where (c.CreateDate >= StartDate && c.CreateDate <= EndDate)
                                    && c.EndDate <= DateTime.Now && (c.ContractName.Contains(search) || c.PaymentAddress.Contains(search)
                                    || c.DeliveryAddress.Contains(search) || c.Status.ToString().Contains(search)
                                    || c.ID.ToString().Contains(search) || c.UserID.ToString().Contains(search)
                                    || c.UpdateBy.ToString().Contains(search) || c.CreateBy.ToString().Contains(search)
                                    )
                                    select new { Contract = c, User = u, OrderDetail = o }
                                   );
                        return Json(new
                        {
                            value = list.ToList(),
                            JsonRequestBehavior.AllowGet
                        });
                    }
                    else if (IDDuration == 2)
                    {
                        var list = (from c in db.Contracts
                                    join o in db.OrderDetails on c.ID equals o.ContractID
                                    join i in db.Insurances on o.InsurantID equals i.ID
                                    join ci in db.InsuranceCategories on i.InsuranceCategoryID equals ci.ID
                                    join u in db.Users on c.UserID equals u.ID
                                    where (c.CreateDate >= StartDate && c.CreateDate <= EndDate)
                                    && (c.EndDate >= DateTime.Now && c.EndDate <= Duration) && (c.ContractName.Contains(search) || c.PaymentAddress.Contains(search)
                                    || c.DeliveryAddress.Contains(search) || c.Status.ToString().Contains(search)
                                    || c.ID.ToString().Contains(search) || c.UserID.ToString().Contains(search)
                                    || c.UpdateBy.ToString().Contains(search) || c.CreateBy.ToString().Contains(search)
                                    )
                                    select new { Contract = c, User = u, OrderDetail = o }
                                   );
                        return Json(new
                        {
                            value = list.ToList(),
                            JsonRequestBehavior.AllowGet
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            value = 0,
                            JsonRequestBehavior.AllowGet
                        });
                    }

                }
                else
                {
                    if (IDDuration == 0)
                    {
                        var list = (from c in db.Contracts
                                    join o in db.OrderDetails on c.ID equals o.ContractID
                                    join i in db.Insurances on o.InsurantID equals i.ID
                                    join ci in db.InsuranceCategories on i.InsuranceCategoryID equals ci.ID
                                    join u in db.Users on c.UserID equals u.ID
                                    where (c.CreateDate >= StartDate && c.CreateDate <= EndDate)
                                    && c.Status == IDStatus && (c.ContractName.Contains(search) || c.PaymentAddress.Contains(search)
                                    || c.DeliveryAddress.Contains(search) || c.Status.ToString().Contains(search)
                                    || c.ID.ToString().Contains(search) || c.UserID.ToString().Contains(search)
                                    || c.UpdateBy.ToString().Contains(search) || c.CreateBy.ToString().Contains(search)
                                    )
                                    select new { Contract = c, User = u, OrderDetail = o }
                                   );
                        return Json(new
                        {
                            value = list.ToList(),
                            JsonRequestBehavior.AllowGet
                        });
                    }
                    else if (IDDuration == 1)
                    {
                        var list = (from c in db.Contracts
                                    join o in db.OrderDetails on c.ID equals o.ContractID
                                    join i in db.Insurances on o.InsurantID equals i.ID
                                    join ci in db.InsuranceCategories on i.InsuranceCategoryID equals ci.ID
                                    join u in db.Users on c.UserID equals u.ID
                                    where (c.CreateDate >= StartDate && c.CreateDate <= EndDate)
                                    && c.EndDate <= DateTime.Now && c.Status == IDStatus && (c.ContractName.Contains(search) || c.PaymentAddress.Contains(search)
                                    || c.DeliveryAddress.Contains(search) || c.Status.ToString().Contains(search)
                                    || c.ID.ToString().Contains(search) || c.UserID.ToString().Contains(search)
                                    || c.UpdateBy.ToString().Contains(search) || c.CreateBy.ToString().Contains(search)
                                    )
                                    select new { Contract = c, User = u, OrderDetail = o }
                                   );
                        return Json(new
                        {
                            value = list.ToList(),
                            JsonRequestBehavior.AllowGet
                        });
                    }
                    else if (IDDuration == 2)
                    {
                        var list = (from c in db.Contracts
                                    join o in db.OrderDetails on c.ID equals o.ContractID
                                    join i in db.Insurances on o.InsurantID equals i.ID
                                    join ci in db.InsuranceCategories on i.InsuranceCategoryID equals ci.ID
                                    join u in db.Users on c.UserID equals u.ID
                                    where (c.CreateDate >= StartDate && c.CreateDate <= EndDate)
                                    && (c.EndDate >= DateTime.Now && c.EndDate <= Duration) && c.Status == IDStatus && (c.ContractName.Contains(search) || c.PaymentAddress.Contains(search)
                                    || c.DeliveryAddress.Contains(search) || c.Status.ToString().Contains(search)
                                    || c.ID.ToString().Contains(search) || c.UserID.ToString().Contains(search)
                                    || c.UpdateBy.ToString().Contains(search) || c.CreateBy.ToString().Contains(search)
                                    )
                                    select new { Contract = c, User = u, OrderDetail = o }
                                   );
                        return Json(new
                        {
                            value = list.ToList(),
                            JsonRequestBehavior.AllowGet
                        });
                    }
                    return Json(new
                    {
                        value = 0,
                        JsonRequestBehavior.AllowGet
                    });

                }

            }
            else
            {
                if (IDStatus == 10)
                {
                    if (IDDuration == 0)
                    {
                        var list = (from c in db.Contracts
                                    join o in db.OrderDetails on c.ID equals o.ContractID
                                    join i in db.Insurances on o.InsurantID equals i.ID
                                    join ci in db.InsuranceCategories on i.InsuranceCategoryID equals ci.ID
                                    join u in db.Users on c.UserID equals u.ID
                                    where ci.ID == ID && (c.CreateDate >= StartDate && c.CreateDate <= EndDate)
                                    && (c.ContractName.Contains(search) || c.PaymentAddress.Contains(search)
                                    || c.DeliveryAddress.Contains(search) || c.Status.ToString().Contains(search)
                                    || c.ID.ToString().Contains(search) || c.UserID.ToString().Contains(search)
                                    || c.UpdateBy.ToString().Contains(search) || c.CreateBy.ToString().Contains(search)
                                    )
                                    select new { Contract = c, User = u, OrderDetail = o });
                        return Json(new
                        {
                            value = list.ToList(),
                            JsonRequestBehavior.AllowGet
                        });
                    }
                    else if (IDDuration == 1)
                    {
                        var list = (from c in db.Contracts
                                    join o in db.OrderDetails on c.ID equals o.ContractID
                                    join i in db.Insurances on o.InsurantID equals i.ID
                                    join ci in db.InsuranceCategories on i.InsuranceCategoryID equals ci.ID
                                    join u in db.Users on c.UserID equals u.ID
                                    where i.InsuranceCategoryID == ID && (c.CreateDate >= StartDate && c.CreateDate <= EndDate)
                                    && c.EndDate <= DateTime.Now && (c.ContractName.Contains(search) || c.PaymentAddress.Contains(search)
                                    || c.DeliveryAddress.Contains(search) || c.Status.ToString().Contains(search)
                                    || c.ID.ToString().Contains(search) || c.UserID.ToString().Contains(search)
                                    || c.UpdateBy.ToString().Contains(search) || c.CreateBy.ToString().Contains(search)
                                    )
                                    select new { Contract = c, User = u, OrderDetail = o });
                        return Json(new
                        {
                            value = list.ToList(),
                            JsonRequestBehavior.AllowGet
                        });
                    }
                    else if (IDDuration == 2)
                    {
                        var list = (from c in db.Contracts
                                    join o in db.OrderDetails on c.ID equals o.ContractID
                                    join i in db.Insurances on o.InsurantID equals i.ID
                                    join ci in db.InsuranceCategories on i.InsuranceCategoryID equals ci.ID
                                    join u in db.Users on c.UserID equals u.ID
                                    where i.InsuranceCategoryID == ID && (c.CreateDate >= StartDate && c.CreateDate <= EndDate)
                                    && (c.EndDate >= DateTime.Now && c.EndDate <= Duration) && (c.ContractName.Contains(search) || c.PaymentAddress.Contains(search)
                                    || c.DeliveryAddress.Contains(search) || c.Status.ToString().Contains(search)
                                    || c.ID.ToString().Contains(search) || c.UserID.ToString().Contains(search)
                                    || c.UpdateBy.ToString().Contains(search) || c.CreateBy.ToString().Contains(search)
                                    )
                                    select new { Contract = c, User = u, OrderDetail = o });
                        return Json(new
                        {
                            value = list.ToList(),
                            JsonRequestBehavior.AllowGet
                        });
                    }
                    return Json(new
                    {
                        value = 0,
                        JsonRequestBehavior.AllowGet
                    });
                }
                else
                {
                    if (IDDuration == 0)
                    {
                        var list = (from c in db.Contracts
                                    join o in db.OrderDetails on c.ID equals o.ContractID
                                    join i in db.Insurances on o.InsurantID equals i.ID
                                    join ci in db.InsuranceCategories on i.InsuranceCategoryID equals ci.ID
                                    join u in db.Users on c.UserID equals u.ID
                                    where i.InsuranceCategoryID == ID && (c.CreateDate >= StartDate && c.CreateDate <= EndDate)
                                    && c.Status == IDStatus && (c.ContractName.Contains(search) || c.PaymentAddress.Contains(search)
                                    || c.DeliveryAddress.Contains(search) || c.Status.ToString().Contains(search)
                                    || c.ID.ToString().Contains(search) || c.UserID.ToString().Contains(search)
                                    || c.UpdateBy.ToString().Contains(search) || c.CreateBy.ToString().Contains(search)
                                    )
                                    select new { Contract = c, User = u, OrderDetail = o });
                        return Json(new
                        {
                            value = list.ToList(),
                            JsonRequestBehavior.AllowGet
                        });
                    }
                    else if (IDDuration == 1)
                    {
                        var list = (from c in db.Contracts
                                    join o in db.OrderDetails on c.ID equals o.ContractID
                                    join i in db.Insurances on o.InsurantID equals i.ID
                                    join ci in db.InsuranceCategories on i.InsuranceCategoryID equals ci.ID
                                    join u in db.Users on c.UserID equals u.ID
                                    where i.InsuranceCategoryID == ID && (c.CreateDate >= StartDate && c.CreateDate <= EndDate)
                                    && c.EndDate <= DateTime.Now && c.Status == IDStatus && (c.ContractName.Contains(search) || c.PaymentAddress.Contains(search)
                                    || c.DeliveryAddress.Contains(search) || c.Status.ToString().Contains(search)
                                    || c.ID.ToString().Contains(search) || c.UserID.ToString().Contains(search)
                                    || c.UpdateBy.ToString().Contains(search) || c.CreateBy.ToString().Contains(search)
                                    )
                                    select new { Contract = c, User = u, OrderDetail = o });
                        return Json(new
                        {
                            value = list.ToList(),
                            JsonRequestBehavior.AllowGet
                        });
                    }
                    else if (IDDuration == 2)
                    {
                        var list = (from c in db.Contracts
                                    join o in db.OrderDetails on c.ID equals o.ContractID
                                    join i in db.Insurances on o.InsurantID equals i.ID
                                    join ci in db.InsuranceCategories on i.InsuranceCategoryID equals ci.ID
                                    join u in db.Users on c.UserID equals u.ID
                                    where i.InsuranceCategoryID == ID && (c.CreateDate >= StartDate && c.CreateDate <= EndDate)
                                    && (c.EndDate >= DateTime.Now && c.EndDate <= Duration) && c.Status == IDStatus && (c.ContractName.Contains(search) || c.PaymentAddress.Contains(search)
                                    || c.DeliveryAddress.Contains(search) || c.Status.ToString().Contains(search)
                                    || c.ID.ToString().Contains(search) || c.UserID.ToString().Contains(search)
                                    || c.UpdateBy.ToString().Contains(search) || c.CreateBy.ToString().Contains(search)
                                    )
                                    select new { Contract = c, User = u, OrderDetail = o });
                        return Json(new
                        {
                            value = list.ToList(),
                            JsonRequestBehavior.AllowGet
                        });
                    }
                    return Json(new
                    {
                        value = 0,
                        JsonRequestBehavior.AllowGet
                    });
                }
            }


        }

        public JsonResult SendMailInfo(int id)
        {
            SendMail sendMail = new SendMail();
            var dao = new ContractDao();
            var result = dao.getOrderDetail(id);
            var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var txtNew = dao.Find(id);

            try
            {
                string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ContractAdmin/ViewMail.cshtml"));
                content = content.Replace("{{ID}}", txtNew.ID.ToString());
                content = content.Replace("{{ContractName}}", txtNew.ContractName);
                content = content.Replace("{{UserID}}", txtNew.UserID.ToString());
                content = content.Replace("{{TotalPrice}}", txtNew.TotalPrice.ToString());
                content = content.Replace("{{StartDate}}", txtNew.StartDate.ToString());
                content = content.Replace("{{EndDate}}", txtNew.EndDate.ToString());
                content = content.Replace("{{PaymentAddress}}", txtNew.PaymentAddress);
                content = content.Replace("{{DeliveryAddress}}", txtNew.DeliveryAddress);
                if (txtNew.Status == 2)
                {
                    content = content.Replace("{{Status}}", "Đang xử lý");
                }
                else if (txtNew.Status == 1)
                {
                    content = content.Replace("{{Status}}", "Hoạt động");
                }
                else
                {
                    content = content.Replace("{{Status}}", "Khóa");
                }

                //orderDetail
                content = content.Replace("{{InsuranceID}}", getOrderDetail.InsurantID.ToString());
                content = content.Replace("{{ContractID}}", getOrderDetail.ContractID.ToString());
                content = content.Replace("{{Quantity}}", getOrderDetail.Quantity.ToString());
                content = content.Replace("{{Price}}", getOrderDetail.Price.ToString());
                content = content.Replace("{{StartDate}}", getOrderDetail.StartDate.ToString());
                content = content.Replace("{{EndDate}}", getOrderDetail.EndDate.ToString());
                content = content.Replace("{{FullName}}", getOrderDetail.FullName.ToString());
                content = content.Replace("{{HomePhone}}", getOrderDetail.HomePhone.ToString());
                content = content.Replace("{{MobilePhone}}", getOrderDetail.MobilePhone.ToString());
                content = content.Replace("{{Email}}", getOrderDetail.Email.ToString());
                content = content.Replace("{{AddressDetail}}", getOrderDetail.AddressDetail.ToString());
                content = content.Replace("{{AddressID}}", getOrderDetail.AddressID.ToString());
                content = content.Replace("{{AddressContract}}", getOrderDetail.AddressContract.ToString());
                content = content.Replace("{{AddressIDContract}}", getOrderDetail.AddressIDContract.ToString());
                content = content.Replace("{{FullNameReceive}}", getOrderDetail.FullNameReceive.ToString());
                content = content.Replace("{{Dob}}", getOrderDetail.Dob.ToString());
                content = content.Replace("{{Identity}}", getOrderDetail.Identity.ToString());
                if (getOrderDetail.Relationship == 1)
                {
                    content = content.Replace("{{Relationship}}", "Your self");
                }
                else if (getOrderDetail.Relationship == 2)
                {
                    content = content.Replace("{{Relationship}}", "Father");
                }
                else if (getOrderDetail.Relationship == 3)
                {
                    content = content.Replace("{{Relationship}}", "Mother");
                }
                else if (getOrderDetail.Relationship == 4)
                {
                    content = content.Replace("{{Relationship}}", "Wife");
                }
                else if (getOrderDetail.Relationship == 5)
                {
                    content = content.Replace("{{Relationship}}", "Husband");
                }
                else if (getOrderDetail.Relationship == 6)
                {
                    content = content.Replace("{{Relationship}}", "Child");
                }
                if (getOrderDetail.Gender == 1)
                {
                    content = content.Replace("{{Gender}}", "Nam");
                }
                else
                {
                    content = content.Replace("{{Gender}}", "Nữ");
                }
                content = content.Replace("{{Job}}", getOrderDetail.Job.ToString());
                content = content.Replace("{{TermInsurance}}", getOrderDetail.TermInsurance.ToString());
                content = content.Replace("{{HousePrice}}", getOrderDetail.HousePrice.ToString());

                sendMail.SendEmail(result, "Bản tin mới từ insurance team", content);

                Session["SendMailSuccsess"] = 1;

                return Json(new
                {
                    status = true
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false
                });
            }


        }

        public JsonResult ChangeSession()
        {
            Session["SendMailSuccsess"] = null;
            Session["SendMailError"] = null;
            return Json(new
            {
                status = true
            });
        }
        public JsonResult ChangeStatus(int id)
        {
            SendMail sendMail = new SendMail();
            var dao = new ContractDao();
            var result1 = dao.getOrderDetail(id);
            var txtNew = dao.Find(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var result = dao.ChangeStatusDone(id, Convert.ToInt32(adminID));
            if (result == true)
            {
                bool history = dao.InsertHistoryContract(id);
                if (history == true)
                {
                    string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ContractAdmin/ViewMailActive.cshtml"));
                    content = content.Replace("{{ID}}", txtNew.ID.ToString());
                    content = content.Replace("{{ContractName}}", txtNew.ContractName);
                    content = content.Replace("{{UserID}}", txtNew.UserID.ToString());
                    content = content.Replace("{{TotalPrice}}", txtNew.TotalPrice.ToString());
                    content = content.Replace("{{StartDate}}", txtNew.StartDate.ToString());
                    content = content.Replace("{{EndDate}}", txtNew.EndDate.ToString());
                    content = content.Replace("{{PaymentAddress}}", txtNew.PaymentAddress);
                    content = content.Replace("{{DeliveryAddress}}", txtNew.DeliveryAddress);
                    if (txtNew.Status == 2)
                    {
                        content = content.Replace("{{Status}}", "Đang xử lý");
                    }
                    else if (txtNew.Status == 1)
                    {
                        content = content.Replace("{{Status}}", "Hoạt động");
                    }
                    else
                    {
                        content = content.Replace("{{Status}}", "Khóa");
                    }
                    //orderDetail
                    content = content.Replace("{{InsuranceID}}", getOrderDetail.InsurantID.ToString());
                    content = content.Replace("{{ContractID}}", getOrderDetail.ContractID.ToString());
                    content = content.Replace("{{Quantity}}", getOrderDetail.Quantity.ToString());
                    content = content.Replace("{{Price}}", getOrderDetail.Price.ToString());
                    content = content.Replace("{{StartDate}}", getOrderDetail.StartDate.ToString());
                    content = content.Replace("{{EndDate}}", getOrderDetail.EndDate.ToString());
                    content = content.Replace("{{FullName}}", getOrderDetail.FullName.ToString());
                    content = content.Replace("{{HomePhone}}", getOrderDetail.HomePhone.ToString());
                    content = content.Replace("{{MobilePhone}}", getOrderDetail.MobilePhone.ToString());
                    content = content.Replace("{{Email}}", getOrderDetail.Email.ToString());
                    content = content.Replace("{{AddressDetail}}", getOrderDetail.AddressDetail.ToString());
                    content = content.Replace("{{AddressID}}", getOrderDetail.AddressID.ToString());
                    content = content.Replace("{{AddressContract}}", getOrderDetail.AddressContract.ToString());
                    content = content.Replace("{{AddressIDContract}}", getOrderDetail.AddressIDContract.ToString());
                    content = content.Replace("{{FullNameReceive}}", getOrderDetail.FullNameReceive.ToString());
                    content = content.Replace("{{Dob}}", getOrderDetail.Dob.ToString());
                    content = content.Replace("{{Identity}}", getOrderDetail.Identity.ToString());
                    if (getOrderDetail.Relationship == 1)
                    {
                        content = content.Replace("{{Relationship}}", "Your self");
                    }
                    else if (getOrderDetail.Relationship == 2)
                    {
                        content = content.Replace("{{Relationship}}", "Father");
                    }
                    else if (getOrderDetail.Relationship == 3)
                    {
                        content = content.Replace("{{Relationship}}", "Mother");
                    }
                    else if (getOrderDetail.Relationship == 4)
                    {
                        content = content.Replace("{{Relationship}}", "Wife");
                    }
                    else if (getOrderDetail.Relationship == 5)
                    {
                        content = content.Replace("{{Relationship}}", "Husband");
                    }
                    else if (getOrderDetail.Relationship == 6)
                    {
                        content = content.Replace("{{Relationship}}", "Child");
                    }
                    if (getOrderDetail.Gender == 1)
                    {
                        content = content.Replace("{{Gender}}", "Nam");
                    }
                    else
                    {
                        content = content.Replace("{{Gender}}", "Nữ");
                    }
                    content = content.Replace("{{Job}}", getOrderDetail.Job.ToString());
                    content = content.Replace("{{TermInsurance}}", getOrderDetail.TermInsurance.ToString());
                    content = content.Replace("{{HousePrice}}", getOrderDetail.HousePrice.ToString());

                    sendMail.SendEmail(result1, "Bản tin mới từ insurance team", content);

                    Session["SendMailSuccsess"] = 1;
                    return Json(new
                    {
                        status = true
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }

        }
        //
        public JsonResult ChangeStatusOpen(int id)
        {
            SendMail sendMail = new SendMail();
            var dao = new ContractDao();
            var result1 = dao.getOrderDetail(id);
            var txtNew = dao.Find(id);
            var admin = (AdminLogin)Session["AdminSession"];
            var adminID = admin.UserID;
            var getOrderDetail = (from c in db.Contracts join o in db.OrderDetails on c.ID equals o.ContractID where c.ID == id select o).FirstOrDefault();
            var result = dao.ChangeStatusOpen(id, Convert.ToInt32(adminID));
            if (result == true)
            {
                bool history = dao.InsertHistoryContract(id);
                if (history == true)
                {
                    string content = System.IO.File.ReadAllText(Server.MapPath("/Areas/AdminManager/Views/ContractAdmin/ViewMailOpenContract.cshtml"));
                    content = content.Replace("{{ID}}", txtNew.ID.ToString());
                    content = content.Replace("{{ContractName}}", txtNew.ContractName);
                    content = content.Replace("{{UserID}}", txtNew.UserID.ToString());
                    content = content.Replace("{{TotalPrice}}", txtNew.TotalPrice.ToString());
                    content = content.Replace("{{StartDate}}", txtNew.StartDate.ToString());
                    content = content.Replace("{{EndDate}}", txtNew.EndDate.ToString());
                    content = content.Replace("{{PaymentAddress}}", txtNew.PaymentAddress);
                    content = content.Replace("{{DeliveryAddress}}", txtNew.DeliveryAddress);
                    if (txtNew.Status == 2)
                    {
                        content = content.Replace("{{Status}}", "Đang xử lý");
                    }
                    else if (txtNew.Status == 1)
                    {
                        content = content.Replace("{{Status}}", "Hoạt động");
                    }
                    else
                    {
                        content = content.Replace("{{Status}}", "Khóa");
                    }

                    //orderDetail
                    content = content.Replace("{{InsuranceID}}", getOrderDetail.InsurantID.ToString());
                    content = content.Replace("{{ContractID}}", getOrderDetail.ContractID.ToString());
                    content = content.Replace("{{Quantity}}", getOrderDetail.Quantity.ToString());
                    content = content.Replace("{{Price}}", getOrderDetail.Price.ToString());
                    content = content.Replace("{{StartDate}}", getOrderDetail.StartDate.ToString());
                    content = content.Replace("{{EndDate}}", getOrderDetail.EndDate.ToString());
                    content = content.Replace("{{FullName}}", getOrderDetail.FullName.ToString());
                    content = content.Replace("{{HomePhone}}", getOrderDetail.HomePhone.ToString());
                    content = content.Replace("{{MobilePhone}}", getOrderDetail.MobilePhone.ToString());
                    content = content.Replace("{{Email}}", getOrderDetail.Email.ToString());
                    content = content.Replace("{{AddressDetail}}", getOrderDetail.AddressDetail.ToString());
                    content = content.Replace("{{AddressID}}", getOrderDetail.AddressID.ToString());
                    content = content.Replace("{{AddressContract}}", getOrderDetail.AddressContract.ToString());
                    content = content.Replace("{{AddressIDContract}}", getOrderDetail.AddressIDContract.ToString());
                    content = content.Replace("{{FullNameReceive}}", getOrderDetail.FullNameReceive.ToString());
                    content = content.Replace("{{Dob}}", getOrderDetail.Dob.ToString());
                    content = content.Replace("{{Identity}}", getOrderDetail.Identity.ToString());
                    if (getOrderDetail.Relationship == 1)
                    {
                        content = content.Replace("{{Relationship}}", "Your self");
                    }
                    else if (getOrderDetail.Relationship == 2)
                    {
                        content = content.Replace("{{Relationship}}", "Father");
                    }
                    else if (getOrderDetail.Relationship == 3)
                    {
                        content = content.Replace("{{Relationship}}", "Mother");
                    }
                    else if (getOrderDetail.Relationship == 4)
                    {
                        content = content.Replace("{{Relationship}}", "Wife");
                    }
                    else if (getOrderDetail.Relationship == 5)
                    {
                        content = content.Replace("{{Relationship}}", "Husband");
                    }
                    else if (getOrderDetail.Relationship == 6)
                    {
                        content = content.Replace("{{Relationship}}", "Child");
                    }
                    if (getOrderDetail.Gender == 1)
                    {
                        content = content.Replace("{{Gender}}", "Nam");
                    }
                    else
                    {
                        content = content.Replace("{{Gender}}", "Nữ");
                    }
                    content = content.Replace("{{Job}}", getOrderDetail.Job.ToString());
                    content = content.Replace("{{TermInsurance}}", getOrderDetail.TermInsurance.ToString());
                    content = content.Replace("{{HousePrice}}", getOrderDetail.HousePrice.ToString());

                    sendMail.SendEmail(result1, "Bản tin mới từ insurance team", content);

                    Session["SendMailSuccsess"] = 1;
                    return Json(new
                    {
                        status = true
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = false
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }

        }
    }
}
