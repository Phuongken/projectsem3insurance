﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;

namespace ProjectInsurance.Areas.AdminManager.Controllers
{
    public class PolicyInsuranceCategoryAdminController : BaseController
    {
        // GET: AdminManager/PolicyInsuranceCategoryAdmin
        

        // GET: AdminManager/PolicyInsuranceCategoryAdmin/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AdminManager/PolicyInsuranceCategoryAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminManager/PolicyInsuranceCategoryAdmin/Create
        [HttpPost]
        public ActionResult Create(PolicyInsuranceCategory policyInsuranceCategory)
        {
            if (ModelState.IsValid)
            {
                var dao = new PolicyInsuranceCategoryDao();
                policyInsuranceCategory.Status = 1;
                bool result = dao.Insert(policyInsuranceCategory);
                if (result == true)
                {
                    return RedirectToAction("Index", "PolicyInsuranceCategoryAdmin");
                }
            }
            return View(policyInsuranceCategory);
        }

        // GET: AdminManager/PolicyInsuranceCategoryAdmin/Edit/5
        public ActionResult Edit(int? id)
        {
            PolicyInsuranceCategoryDao dao = new PolicyInsuranceCategoryDao();
            var policyInsuranceCategory = dao.find(id);
            return View(policyInsuranceCategory);
        }

        // POST: AdminManager/PolicyInsuranceCategoryAdmin/Edit/5
        [HttpPost]
        public ActionResult Edit(PolicyInsuranceCategory policyInsuranceCategory)
        {
            if (ModelState.IsValid)
            {
                var dao = new PolicyInsuranceCategoryDao();
                var result = dao.Update(policyInsuranceCategory);
                if (result == true)
                {
                    return RedirectToAction("Index", "PolicyInsuranceCategoryAdmin");
                }
                else
                {
                    ModelState.AddModelError("", "Chỉnh sửa thông tin thất bại");
                }
            }
            return View(policyInsuranceCategory);
        }

        // GET: AdminManager/PolicyInsuranceCategoryAdmin/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AdminManager/PolicyInsuranceCategoryAdmin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
