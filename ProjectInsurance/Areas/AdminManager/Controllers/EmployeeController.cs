﻿using Model.Dao;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Areas.AdminManager.Controllers
{
    public class EmployeeController : BaseController
    {
        // GET: AdminManager/Employee
        public ActionResult Index()
        {
            AdminDao dao = new AdminDao();
            var emp = dao.listEmployee();
            return View(emp);
        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            AdminDao dao = new AdminDao();
            var emp = dao.find(id);
            return View(emp);
        }
        [HttpPost]
        public ActionResult Edit(Employee emp)
        {
     
                if (ModelState.IsValid)
                {
                
                var dao = new AdminDao();
                    var result = dao.Edit(emp);
                    if (result == true)
                    {
                        return RedirectToAction("Index","Employee");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Chỉnh sửa thông tin thất bại");
                    }
                }
            return View(emp);
        }
           
        
        public JsonResult Delete(int? id)
        {

            var dao = new AdminDao();
            var result = dao.Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });

            }

        }
        public ActionResult Details(int? id)
        {
            DataModel db = new DataModel();
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Employee emp = db.Employees.Find(id);
            if (emp == null)
                return HttpNotFound();
            return View(emp);
        }
       
    }


}