﻿using Model.Dao;
using Model.EF;
using ProjectInsurance.Areas.AdminManager.Models;
using ProjectInsurance.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Areas.AdminManager.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet]
        // GET: AdminManager/Login
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login(LoginModelAdmin admin)
        {
            if (ModelState.IsValid)
            {
                var dao = new AdminDao();
                var result = dao.Login(admin.Username, admin.Password);
                if (result == true)
                {
                    var cateInsurance = new InsuranceCategoryDao();
                    var cateNew = new NewCategoryDao();
                    var loginAdmin = dao.GetById(admin.Username);
                    var adminSession = new AdminLogin();
                    adminSession.UserName = loginAdmin.Username;
                    adminSession.UserID = loginAdmin.ID;
                    Session["AdminSession"] = adminSession;
                    Session["cateNewID"] = cateNew.ViewBagNew();
                    Session["cateInsuranceID"] = cateInsurance.viewbagIndex();
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Tài khoản hoặc mật khẩu chưa chính xác");
                }

            }
            return View("Index");
        }
        public ActionResult LogOut()
        {
            Session.Add(AdminSession.USER_SESSION, null);
            return Redirect("/admin/login");
        }
    }
}