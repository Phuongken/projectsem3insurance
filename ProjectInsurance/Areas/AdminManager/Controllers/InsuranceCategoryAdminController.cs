﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;

namespace ProjectInsurance.Areas.AdminManager.Controllers
{
    public class InsuranceCategoryAdminController : BaseController
    {
        // GET: AdminManager/InsuranceCategory/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AdminManager/InsuranceCategory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminManager/InsuranceCategory/Create
        [HttpPost]
        public ActionResult Create(InsuranceCategory newInsuranceCategory)
        {
            if (ModelState.IsValid)
            {
                var dao = new InsuranceCategoryDao();
                newInsuranceCategory.Status = 1;
                bool result = dao.Insert(newInsuranceCategory);
                if (result == true)
                {
                    return RedirectToAction("Index", "InsuranceCategoryAdmin");
                }
            }
            return View(newInsuranceCategory);
        }

        // GET: AdminManager/InsuranceCategory/Edit/5
        public ActionResult Edit(int? id)
        {
            InsuranceCategoryDao dao = new InsuranceCategoryDao();
            var insuranceCategory = dao.find(id);
            return View(insuranceCategory);
        }

        // POST: AdminManager/InsuranceCategory/Edit/5
        [HttpPost]
        public ActionResult Edit(InsuranceCategory insuranceCategory)
        {
            if (ModelState.IsValid)
            {
                var dao = new InsuranceCategoryDao();
                var result = dao.Update(insuranceCategory);
                if (result == true)
                {
                    return RedirectToAction("Index", "InsuranceCategoryAdmin");
                }
                else
                {
                    ModelState.AddModelError("", "Chỉnh sửa thông tin thất bại");
                }
            }
            return View(insuranceCategory);
        }

        // POST: AdminManager/InsuranceCategory/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            InsuranceCategoryDao insuranceCategory = new InsuranceCategoryDao();
            var result = insuranceCategory.Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
    }
}
