﻿using Model.Dao;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Areas.AdminManager.Controllers
{
    public class UserController : Controller
    {
        // GET: AdminManager/User
        public ActionResult Index()
        {
            UserDao dao = new UserDao();
            var user = dao.ListUser();
            return View(user);
        }
        // GET: AdminManager/User/Details/5
        public ActionResult Details(int? id)
        {

            DataModel db = new DataModel();
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            User user = db.Users.Find(id);
            if (user == null)
                return HttpNotFound();
            return View(user);
        }

        // GET: AdminManager/User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminManager/User/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AdminManager/User/Edit/5
        public ActionResult Edit(int id)
        {
            UserDao dao = new UserDao();
            var user = dao.find(id);
            return View(user);
        }

        // POST: AdminManager/User/Edit/5
        [HttpPost]
        public ActionResult Edit(User user)
        {
            if (ModelState.IsValid)
            {
                var dao = new UserDao();
                var result = dao.Edit(user);
                if (result == true)
                {
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    ModelState.AddModelError("", "Chỉnh sửa thông tin thất bại");
                }
            }
            return View(user);
        }


        public JsonResult Delete(int? id)
        {

            var dao = new UserDao();
            var result = dao.Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }

       

    }
}
