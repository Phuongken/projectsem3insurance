﻿using Model.Dao;
using Model.EF;
using Newtonsoft.Json;
using ProjectInsurance.Areas.AdminManager.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Areas.AdminManager.Controllers
{
    public class ChartController : BaseController
    {

        public JsonResult GetJsonData(string startDate, string endDate)
        {

            List<DataPoint> dataPoints = new List<DataPoint>();

            ChartDao dao = new ChartDao();
            foreach (var item in dao.ListInsurance())
            {
                var result = dao.CountOrderID(item.ID, startDate, endDate);
                dataPoints.Add(new DataPoint(item.Name, result));
            }

            return Json(dataPoints.OrderByDescending(x => x.Amount), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetJsonData1(string startDate, string endDate)
        {
            List<DataPoint> dataPoints = new List<DataPoint>();

            ChartDao dao = new ChartDao();
            foreach (var item in dao.ListInsurance())
            {
                var result = dao.CountOrderIDOn(item.ID, startDate, endDate);
                dataPoints.Add(new DataPoint(item.Name, result));
            }

            return Json(dataPoints.OrderByDescending(x => x.Amount), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Revenue(string startDate, string endDate)
        {
            DataModel db = new DataModel();
            var StartDate = Convert.ToDateTime(startDate);
            var EndDate = Convert.ToDateTime(endDate);

            var result = db.HistoryContracts.Where(x => x.Status == 1 && x.UpdateDate >= StartDate && x.UpdateDate <= EndDate).GroupBy(x => DbFunctions.TruncateTime(x.UpdateDate))
               .Select(x => new
               {
                   Total = x.Sum(t => t.TotalPrice),
                   Date = x.Key
               }).OrderBy(d => d.Date).ToList();
            return Json(JsonConvert.SerializeObject(result), "application/json", JsonRequestBehavior.AllowGet);
        }
    }
}