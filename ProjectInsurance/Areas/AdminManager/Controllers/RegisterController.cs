﻿using Model.Dao;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectInsurance.Areas.AdminManager.Controllers
{
    public class RegisterController : Controller
    {
        [HttpGet]
        // GET: AdminManager/Register
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(Admin admin)
        {

            if (ModelState.IsValid)
            {
                var dao = new AdminDao();
                var result = dao.Register(admin);
                if (result == true)
                {
                  //  ViewBag.SuccesMessage("Bạn đã tạo tài khoản thành công");
                    return RedirectToAction("Index", "Login");
                }
                else
                {
                    //ViewBag.DuplicateMessage("Tài khoản này đã tồn tại");
                        return View("Index");
                    
                }

            }
            return View("Index");
        }


    }
}