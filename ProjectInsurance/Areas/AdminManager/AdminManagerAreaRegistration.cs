﻿using System.Web.Mvc;

namespace ProjectInsurance.Areas.AdminManager
{
    public class AdminManagerAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AdminManager";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {

            context.MapRoute(
                name: "Contract",
                url: "hop-dong",
                defaults: new { controller = "ContractAdmin", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Areas.AdminManager.Controllers" }
            );

            context.MapRoute(
                name: "Home Admin",
                url: "trang-chu-admin",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Areas.AdminManager.Controllers" }
            );

            context.MapRoute(
                name: "List User",
                url: "danh-sach-khach-hang",
                defaults: new { controller = "User", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Areas.AdminManager.Controllers" }
            );

            context.MapRoute(
                name: "Add Employee",
                url: "them-moi-nhan-vien",
                defaults: new { controller = "EmployeeRegister", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Areas.AdminManager.Controllers" }
            );

            context.MapRoute(
                name: "List Employee",
                url: "danh-sach-nhan-vien",
                defaults: new { controller = "Employee", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Areas.AdminManager.Controllers" }
            );

            context.MapRoute(
                name: "New Category",
                url: "de-an-chien-luoc/{id}",
                defaults: new { controller = "NewAdmin", action = "NewCate", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Areas.AdminManager.Controllers" }
            );

            context.MapRoute(
                name: "Insurance Category",
                url: "chi-tiet/{id}",
                defaults: new { controller = "InsuranceAdmin", action = "InsuranceCategory", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Areas.AdminManager.Controllers" }
            );

            context.MapRoute(
                name: "Logout admin",
                url: "logout",
                defaults: new { controller = "Login", action = "LogOut", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Areas.AdminManager.Controllers" }
            );

            context.MapRoute(
               name: "Login admin",
               url: "admin/login",
               defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "ProjectInsurance.Areas.AdminManager.Controllers" }
           );

            context.MapRoute(
                "AdminManager_default",
                "AdminManager/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ProjectInsurance.Areas.AdminManager.Controllers" }
            );
        }
    }
}