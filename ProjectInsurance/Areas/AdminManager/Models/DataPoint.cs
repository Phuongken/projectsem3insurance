﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ProjectInsurance.Areas.AdminManager.Models
{
    public class DataPoint
    {
        public DataPoint(string name, int amount)
        {
            this.Name = name;
            this.Amount = amount;
        }
        
        public string Name { get; set; }
        public int Amount { get; set; }
    }
}